#version 150

uniform sampler2DRect tex0;
//uniform sampler2DRect tex1;

uniform float ublurAmnt;
uniform float usigma;
//uniform float uerodeAmnt;
//uniform float uerodeThres;

in vec2 texCoordVarying;
out vec4 outputColor;

const int maxsize = 2000;
// Gaussian weights from http://dev.theomader.com/gaussian-kernel-calculator/

float normpdf(in float x, in float sigma)
{
        //return 0.39894*exp(-0.5*x*x/(sigma*sigma))/sigma;
    return 0.39894*exp(-0.5*x*x/(sigma*sigma))/sigma;
}

float getBrightness(vec4 c)
{
        float max = c.r;
        if(c.g > max) {
                max = c.g;
        }
        if(c.b > max) {
                max = c.b;
        }
        return max;
}

float getLightness(vec4 c)
{
        return (c.r + c.g + c.b) / 3.f;
}

void main()
{
    vec4 color = vec4(0.0, 0.0, 0.0, 0.0);

    int mSize = int(ublurAmnt+0.5);
    mSize = clamp(mSize, 1, maxsize-1);
    int kSize = (mSize-1)/2;
    float kernel[maxsize];

    //create the 1-D kernel
    float Z = 0.0;
    for (int j = 0; j <= kSize; ++j)
    {
        kernel[kSize+j] = kernel[kSize-j] = normpdf(float(j), usigma);
    }

    /*
    int mSize_erode = int(uerodeAmnt+0.5);
    mSize_erode = clamp(mSize_erode, 1, maxsize-1);
    int kSize_erode = (mSize_erode-1)/2;
    float kernel_erode[maxsize];
    for (int j = 0; j <= kSize_erode; ++j)
    {
        kernel_erode[kSize_erode+j] = kernel_erode[kSize_erode-j] = 1.0;
    }

    //read out the texels

    float ref_erode = getLightness(texture(tex0, texCoordVarying));
    for (int j=-kSize_erode; j <= kSize_erode; ++j)
    {
        float k = kernel_erode[kSize_erode+j];
        float erode = getLightness(k*texture(tex0, texCoordVarying + vec2(float(j), 0.0)));
        if(abs(erode - ref_erode) > uerodeThres)
            discard;
    }*/

    //read out the texels

    for (int j=-kSize; j <= kSize; ++j)
    {
        float k = kernel[kSize+j];
        Z += k;
        vec4 gaus = k*texture(tex0, texCoordVarying + vec2(float(j), 0.0));
        color += gaus;
    }

    outputColor = vec4(color/Z);
    outputColor.a = 1.0;
}
