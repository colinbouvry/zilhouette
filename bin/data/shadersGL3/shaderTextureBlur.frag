#version 150

//https://gist.github.com/mantissa/b9aac8e40d679f2d2a7908a12028f299

const int size = 12;

uniform sampler2DRect tex0; // CRS
uniform sampler2DRect tex1; // DEPTHMAP
//uniform sampler2DArray texture; // IMAGES FLOU
uniform sampler2DRect texblur0;
uniform sampler2DRect texblur1;
uniform sampler2DRect texblur2;
uniform sampler2DRect texblur3;
uniform sampler2DRect texblur4;
uniform sampler2DRect texblur5;
uniform sampler2DRect texblur6;
uniform sampler2DRect texblur7;
uniform sampler2DRect texblur8;
uniform sampler2DRect texblur9;
uniform sampler2DRect texblur10;
uniform sampler2DRect texblur11;

uniform float ublurAmnt;

in vec2 texCoordVarying;
out vec4 outputColor;

float getBrightness(vec4 c)
{
        float max = c.r;
        if(c.g > max) {
                max = c.g;
        }
        if(c.b > max) {
                max = c.b;
        }
        return max;
}

float getLightness(vec4 c)
{
        return (c.r + c.g + c.b) / 3.f;
}

void main()
{
    vec4 tex[size];
    //vec2 coord = vec2(1.0 - texCoordVarying.x, texCoordVarying.y); //flip
    tex[0] = texture(texblur0, texCoordVarying);
    tex[1] = texture(texblur1, texCoordVarying);
    tex[2] = texture(texblur2, texCoordVarying);
    tex[3] = texture(texblur3, texCoordVarying);
    tex[4] = texture(texblur4, texCoordVarying);
    tex[5] = texture(texblur5, texCoordVarying);
    tex[6] = texture(texblur6, texCoordVarying);
    tex[7] = texture(texblur7, texCoordVarying);
    tex[8] = texture(texblur8, texCoordVarying);
    tex[9] = texture(texblur9, texCoordVarying);
    tex[10] = texture(texblur10, texCoordVarying);
    tex[11] = texture(texblur11, texCoordVarying);

    vec4 blurTex = texture(tex1, texCoordVarying);

    float blur = getLightness(blurTex) * ublurAmnt; // 0 et 1.0
    //blur = clamp(blur, 0.0, float(size) - 0.001);

    float blursized = blur * float(size);
    float fr = fract(blursized);
    int fl = int(floor(blursized));
    fl = clamp(fl, 0, size -2);

    vec4 color_fl = tex[fl];
    vec4 color_fl1 = tex[fl+1];

    outputColor = mix(color_fl, color_fl1, fr);
    outputColor.a = 1.0;
}
