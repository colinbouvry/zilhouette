#version 150

uniform sampler2DRect tex0;
uniform sampler2DRect tex1;

uniform float ublurAmnt;
uniform float usigma;

in vec2 texCoordVarying;
out vec4 outputColor;

const int maxsize = 2000;
// Gaussian weights from http://dev.theomader.com/gaussian-kernel-calculator/

float normpdf(in float x, in float sigma)
{
        return 0.39894*exp(-0.5*x*x/(sigma*sigma))/sigma;
}

float getBrightness(vec4 c)
{
        float max = c.r;
        if(c.g > max) {
                max = c.g;
        }
        if(c.b > max) {
                max = c.b;
        }
        return max;
}

float getLightness(vec4 c)
{
        return (c.r + c.g + c.b) / 3.f;
}

void main()
{
    vec4 color = vec4(0.0, 0.0, 0.0, 0.0);
    vec4 blurTex = texture(tex1, texCoordVarying);

    float blur = (1.0 - getLightness(blurTex)) * ublurAmnt;

    int mSize = int(blur+0.5);
    mSize = clamp(mSize, 1, maxsize-1);

    int kSize = (mSize-1)/2;

    float kernel[maxsize];

    //create the 1-D kernel
    float sigma = clamp(usigma + 0.2 * ublurAmnt, 2.0, 500.0) ;
    float Z = 0.0;
    for (int j = 0; j <= kSize; ++j)
    {
        kernel[kSize+j] = kernel[kSize-j] = normpdf(float(j), sigma);
    }

    //read out the texels
    for (int j=-kSize; j <= kSize; ++j)
    {
        float k = kernel[kSize+j];
        Z += k;
        color += k*texture(tex0, texCoordVarying + vec2(float(j), 0.0));
    }

    outputColor = vec4(color/Z);
    outputColor.a = 1.0;


    /*
    color += 0.000229 * texture(tex0, texCoordVarying + vec2(blur * -4.0, 0.0));
    color += 0.005977 * texture(tex0, texCoordVarying + vec2(blur * -3.0, 0.0));
    color += 0.060598 * texture(tex0, texCoordVarying + vec2(blur * -2.0, 0.0));
    color += 0.241732 * texture(tex0, texCoordVarying + vec2(blur * -1.0, 0.0));

    color += 0.382928 * texture(tex0, texCoordVarying + vec2(0.0, 0.0));

    color += 0.241732 * texture(tex0, texCoordVarying + vec2(blur * 1.0, 0.0));
    color += 0.060598 * texture(tex0, texCoordVarying + vec2(blur * 2.0, 0.0));
    color += 0.005977 * texture(tex0, texCoordVarying + vec2(blur * 3.0, 0.0));
    color += 0.000229 * texture(tex0, texCoordVarying + vec2(blur * 4.0, 0.0));
    
    outputColor = color;

    */
}
