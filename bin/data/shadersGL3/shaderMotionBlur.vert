#version 430 core

// these are for the programmable pipeline system
uniform mat4 modelViewProjectionMatrix;
uniform mat4 modelViewMatrix;
uniform mat4 textureMatrix;

in vec4 position;
in vec2 texcoord;
in vec4 color;

out vec2 texCoordVarying;
out float distToCameraVarying;
out vec4 colorVarying;

void main()
{
    texCoordVarying = texcoord;
    colorVarying = color;
    gl_Position = modelViewProjectionMatrix * position;
}
