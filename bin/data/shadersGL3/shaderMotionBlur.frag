#version 430 core

uniform sampler2DRect tex0;
uniform sampler2DRect tex1;
uniform sampler2DRect tex2;
uniform sampler2DRect tex3;
uniform sampler2DRect tex4;
uniform sampler2DRect tex5;
uniform sampler2DRect tex6;
uniform sampler2DRect tex7;

uniform int usize;

uniform vec4 ucolor;

out vec4 outputColor;

in vec2 texCoordVarying;
in vec4 colorVarying;



void main()
{
    vec4 blur = vec4(0.0);
    vec4 color[8];

    color[0] = texture(tex0, texCoordVarying);
    color[1] = texture(tex1, texCoordVarying);
    color[2] = texture(tex2, texCoordVarying);
    color[3] = texture(tex3, texCoordVarying);
    color[4] = texture(tex4, texCoordVarying);
    color[5] = texture(tex5, texCoordVarying);
    color[6] = texture(tex6, texCoordVarying);
    color[7] = texture(tex7, texCoordVarying);


    for (int i =0 ; i < usize ; ++i) {
        blur += color[i];
    }
    outputColor = blur / vec4(usize);
    outputColor.a = 1.0;
}
