#version 430 core

// these are for the programmable pipeline system
uniform mat4 modelViewProjectionMatrix;
uniform mat4 modelViewMatrix;
uniform mat4 textureMatrix;
uniform int ortho;

in vec4 position;
in vec2 texcoord;
in vec4 color;

out vec2 texCoordVarying;
out float distToCameraVarying;
out vec4 colorVarying;

void main()
{
    texCoordVarying = texcoord;
    colorVarying = color;
    gl_Position = modelViewProjectionMatrix * position;

    //vec4 position_model = modelViewMatrix * position;
    //if(position.z < 0.0)
    //    colorVarying.a = 0.0;

    // perpective
    if(ortho==0)
        distToCameraVarying = gl_Position.w;
    else
        // ortho
        //distToCameraVarying = gl_Position.z;
        //https://community.khronos.org/t/how-does-gl-position-zw-effect-zbuffer/69496/2
        distToCameraVarying = 0.5 * gl_Position.z / gl_Position.w + 0.5;
}
