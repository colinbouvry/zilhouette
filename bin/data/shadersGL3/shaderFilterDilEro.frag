#version 150

uniform sampler2DRect tex0;
//uniform sampler2DRect tex1;

uniform float ublurAmnt;
uniform int uDilate;

in vec2 texCoordVarying;
out vec4 outputColor;

const int maxsize = 2000;

float getLightness(vec4 c)
{
        return (c.r + c.g + c.b) / 3.f;
}

void main()
{
    vec4 color = texture(tex0, texCoordVarying);

    int mSize = int(ublurAmnt+0.5);
    mSize = clamp(mSize, 1, maxsize-1);
    int kSize = (mSize-1)/2;
    //float kernel[maxsize][maxsize];
    float minmax = 1.0;
    if(uDilate > 0.0)
       minmax = 0.0;

    //create the 1-D kernel
    /*
    float Z = 0.0;
    for (int i = 0; i <= kSize; ++i)
    {
        for (int j = 0; j <= kSize; ++j)
        {
            kernel[kSize+i][kSize+j] = kernel[kSize-i][kSize-j] = 1.0; //normpdf(float(j), usigma);
        }
    }*/

    //read out the texels
    for (int i=-kSize; i <= kSize; ++i)
    {
        for (int j=-kSize; j <= kSize; ++j)
        {
            //float k = kernel[kSize+j];
            //Z += k;
            vec4 color_conv = texture(tex0, texCoordVarying + vec2(float(i), float(j)));
            float l = getLightness(color_conv);
            if(uDilate > 0.0) {
                if(l > minmax) {
                    minmax = l;
                    color = color_conv;
                }
            } else {
                if(l < minmax) {
                    minmax = l;
                    color = color_conv;
                }
            }
        }
    }

    outputColor = color; //vec4(color/Z);
    outputColor.a = 1.0;
}
