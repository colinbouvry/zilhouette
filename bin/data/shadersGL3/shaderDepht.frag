#version 430 core

uniform float urangeFactor;
uniform float urangeMin;
uniform vec4 ucolor;
uniform int colormode;

out vec4 outputColor;

in vec2 texCoordVarying;
in float distToCameraVarying;
in vec4 colorVarying;

vec3 rgb2hsv(vec3 c)
{
    vec4 K = vec4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);
    vec4 p = mix(vec4(c.bg, K.wz), vec4(c.gb, K.xy), step(c.b, c.g));
    vec4 q = mix(vec4(p.xyw, c.r), vec4(c.r, p.yzx), step(p.x, c.r));

    float d = q.x - min(q.w, q.y);
    float e = 1.0e-10;
    return vec3(abs(q.z + (q.w - q.y) / (6.0 * d + e)), d / (q.x + e), q.x);
}

vec3 hsv2rgb(vec3 c)
{
    vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
    vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
    return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
}

float circularOut(float t) {
  return sqrt((2.0 - t) * t);
}

float circularIn(float t) {
  return 1.0 - sqrt(1.0 - t * t);
}

void main()
{
    if (colorVarying.a < 0.01)
        discard;

    vec2 coord = gl_PointCoord;
    vec2 uv = coord - vec2(0.5);
    float fact = 1.0 - 2.0 * length(uv);
    if(fact < 0.05)
        discard;

    //float d = (distToCameraVarying - urangeMin) * urangeFactor/ 100.0  ;
    float d = 1.0 - distToCameraVarying * urangeFactor - urangeMin;
    if(colormode == 0)
        outputColor = vec4(d) * ucolor * colorVarying;
    else
        outputColor = vec4(hsv2rgb(vec3(clamp(d,0.0,1.0),1.0,1.0)),1.0);

    outputColor.a = 1.0; //fact;
}
