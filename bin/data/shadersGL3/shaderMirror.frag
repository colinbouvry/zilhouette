#version 150

uniform sampler2DRect tex0; // DEPTHMAP

uniform float offsetY;

in vec2 texCoordVarying;
out vec4 outputColor;


void main()
{
    vec4 color = texture(tex0, texCoordVarying);
    if (offsetY >= 0.0 && texCoordVarying.y > offsetY) {
        color = texture(tex0, vec2(texCoordVarying.x, -texCoordVarying.y + 2 * offsetY));
    }

    outputColor = color;
    outputColor.a = 1.0;
}
