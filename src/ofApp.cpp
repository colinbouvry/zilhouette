#include "ofApp.h"

bool ofApp::show_app_log = false;
bool ofApp::show_landmarks = true;
bool ofApp::show_perceptions = true;
bool ofApp::show_archis = true;
std::shared_ptr<ofxImGui::LoggerChannel> ofApp::sChannel = nullptr;

ofApp::ofApp()
    : mPointSize(5.f)
    , mDrawGrid(true)
    , mRangeFactor(25.f)
    , mRangeMin(0.f)
    , mAlphaArchi(255)
    , mVisibleRenderWin(true)
    , mStepfRenderWin(1.f)
    , mPositionRenderWin(glm::vec2(0.f, 0.f))
    , mScaleRenderWin(glm::vec2(1.f, 1.f))
    , mShiftRenderWin(glm::vec4(0.f, 0.f, 1.f, 1.f))
    , mCenterScreen(glm::vec3(-4.37*0.5, 2.34*0.5, 0.05))
    , mNearDephtMap(0.0001f)
    , mFarDephtMap(1000.f)
    , mOrthoScale(10.0)
    , mBGui(false)
{
    loadXml();
    p_open_render = new bool(true);
}

ofApp::~ofApp() {
    for(int i = 0 ; i < mPerceptions.size() ; ++i) {
        delete mPerceptions[i];
    }
}

//--------------------------------------------------------------
void ofApp::setupRender() {

}

//--------------------------------------------------------------
void ofApp::setup(){

    ofSetVerticalSync(true);
#ifdef ZILHOUETTE_ROS
    ROS_INFO("setup");
#endif
    ofEnableDepthTest();
    glEnable(GL_POINT_SMOOTH); // use circular points instead of square points

    // GUI
    mGui.setup();
    ImGuiIO& io = ImGui::GetIO();
    io.MouseDrawCursor = false;
    //io.DisplayFramebufferScale = {2, 2};

    // SCALE
#ifdef ZILHOUETTE_PROD4K
    io.FontGlobalScale = 1.5f;
    ImGui::GetStyle().ScaleAllSizes(1.5f);
#else
	io.FontGlobalScale = 1.f;
	ImGui::GetStyle().ScaleAllSizes(1.f);
#endif

    // LOAD OBJET
    loadObjetsXml();

    // CAM
    // pers
    ofCam::setupPers(mCamPers);

    // top
    ofCam::setupTop(mCamTop);

    // left
    ofCam::setupLeft(mCamLeft);

    // front
    ofCam::setupFront(mCamFront);

    // deph map
    ofCam::setupDephMap(mCamDephMap);
    mCamDephMap.mCam.setNearClip(mNearDephtMap);
    mCamDephMap.mCam.setFarClip(mFarDephtMap);


    //mCamOrtho.setScale(10000.f);
    // GRID
    mAngleGrid = glm::vec3(0.f, 0.f, 90.f);
    mPositionGrid = glm::vec3(-2.f, 0.f, 3.f);

    // BLUR
    mBlur.setup();

    mShaderDepth.load("shadersGL3/shaderDepht");

    // Logger
    // TO DO TEST
    // https://github.com/jvcleave/ofxImGui/pull/90
    // https://github.com/hku-ect/NatNet2OSCbridge/blob/a1d4b150821564a3f01ab5d78276fda867d6fd5b/src/uiWidgets.cpp
    sChannel = std::shared_ptr<ofxImGui::LoggerChannel>(new ofxImGui::LoggerChannel());
    sChannel->capture_stdout();
}

//--------------------------------------------------------------
void ofApp::update() {
#ifdef ZILHOUETTE_ROS 
    ros::spinOnce();
#endif
    for(auto& perception : mPerceptions) {
        perception->update();
    }

    bool iawh = mBGui && ImGui::IsAnyWindowHovered();
    mCamPers.update(iawh);
    mCamTop.update(iawh);
    mCamLeft.update(iawh);
    mCamDephMap.update(iawh);
    mCamFront.update(iawh);
}

void ofApp::renderScene(const ofCam& cam, bool visible_depthmap) {

    ofEnableDepthTest();
    // draw grid
    if(mDrawGrid) {
        ofSetColor(ofColor(255, 255, 255, 255));
        ofPushView();

        ofMatrix4x4 mat;
        mat.preMultTranslate(mPositionGrid);
        mat.preMultRotate(ofQuaternion(mAngleGrid.x, ofVec3f(1, 0, 0), mAngleGrid.y, ofVec3f(0, 1, 0), mAngleGrid.z, ofVec3f(0, 0, 1)));

        ofMultMatrix(mat);
        //ofDrawGrid(1.f, 4, true,true,true,true);
        ofDrawGridPlane(1.f, 4, true);
        ofPopView();
    }

    // draw axis
    ofPushView();
    ofTranslate(0.f,0.f,0.f);
    ofDrawAxis(1.f);
    ofPopView();

    // draw points
    glPointSize(mPointSize); // make the points bigger
    for(auto& perception : mPerceptions) {
        perception->draw();
    }

    // landmarks
    for (auto& land : mLandmarks)
    {
        land->draw();
    }

    if(visible_depthmap)
        renderSceneDephtMap(cam);
}

void ofApp::renderSceneDephtMap(const ofCam& cam) {

    ofEnableDepthTest();
    ofEnableAlphaBlending();


    for(auto&  archi : mArchis) {
        archi->draw();
    }

    glDepthMask(GL_TRUE);
    //glDepthFunc(GL_LEQUAL);
    mShaderDepth.begin();


    //glDepthMask(false);
    //float range = mRangeFactor;
    //if(cam.mName != "DEPHTMAP")
    //       range /= 100.f;

    float range = 1.f/cam.mRangeFactor;
    mShaderDepth.setUniform1f("urangeFactor", range);

    float rangemin = mRangeMin;
    //if(cam.mName == "DEPHTMAP")
    //    rangemin += 140.f; //glm::length(mCenterScreen - cam.mPosition);
    mShaderDepth.setUniform1f("urangeMin", rangemin);

    // draw points
    glPointSize(mPointSize); // make the points bigger
    for(auto& perception : mPerceptions) {
        ofColor color = ofColor(255, 255, 255,255);
        if(cam.mName != "DEPHTMAP") {
            color = perception->getColor();
            mShaderDepth.setUniform1i("ortho",0);
            mShaderDepth.setUniform1i("colormode", perception->getColorMode());
        }
        else {
            mShaderDepth.setUniform1i("ortho",1);
            mShaderDepth.setUniform1i("colormode", 0);
        }
        mShaderDepth.setUniform4f("ucolor", color);
        perception->drawMesh();
    }

    mShaderDepth.end();
}

//--------------------------------------------------------------
void ofApp::drawRender(ofEventArgs & args) {
    ofEnableAlphaBlending();


    // COLOR
    ofSetColor(ofColor(255, 255, 255, 255));

    // BACKGROUND
    ofBackgroundGradient(ofColor::gray, ofColor::black, OF_GRADIENT_CIRCULAR);

    // disable depth test
    ofDisableDepthTest();

    // blur draw
    if(mVisibleRenderWin) {
        mBlur.drawResult(mScaleRenderWin, mPositionRenderWin);
    }

    // draw rectangle
    ofSetColor(ofColor(0, 0, 0, 255));
    ofDrawRectangle(0.f, 0.f, mShiftRenderWin.x * RENDER_WINDOW_SIZE_X, RENDER_WINDOW_SIZE_Y);
    ofDrawRectangle(0.f, 0.f, RENDER_WINDOW_SIZE_X, mShiftRenderWin.y * RENDER_WINDOW_SIZE_Y);
    ofDrawRectangle(mShiftRenderWin.z * RENDER_WINDOW_SIZE_X, 0.f, (1.f - mShiftRenderWin.z) * RENDER_WINDOW_SIZE_X, RENDER_WINDOW_SIZE_Y);
    ofDrawRectangle(0.f, mShiftRenderWin.w * RENDER_WINDOW_SIZE_Y, RENDER_WINDOW_SIZE_X, (1.f - mShiftRenderWin.w) * RENDER_WINDOW_SIZE_Y);
}

//--------------------------------------------------------------
void ofApp::draw(){
    ofEnableAlphaBlending();

    // COLOR
    ofSetColor(ofColor(255, 255, 255, 255));

    // BACKGROUND
    ofBackgroundGradient(ofColor::gray, ofColor::black, OF_GRADIENT_CIRCULAR);

    // DEPTH
    ofEnableDepthTest();

    // FBO DEPHMAP
    mBlur.dephBegin();
        ofClear(0,0,0,255);
        ofPushMatrix();
        mCamDephMap
                .begin(
                    ofRectangle(glm::vec2(0.f), mBlur.getSize())
                );
        ofScale(mCamDephMap.getScale());
        ofEnableDepthTest();
        renderSceneDephtMap(mCamDephMap);
        ofPopMatrix();
		mCamDephMap.end();
    mBlur.dephEnd();

    // CAM PERSPECTIVE
    mCamPers.begin();
    ofPushView();
    ofScale(mCamPers.getScale());
    renderScene(mCamPers, mCamPers.getVisibleMesh());
    ofPopView();
    mCamPers.end();

    // CAM TOP
    mCamTop.begin();
    ofPushView();
    ofScale(mCamTop.getScale());
    renderScene(mCamTop, mCamTop.getVisibleMesh());
    ofPopView();
    mCamTop.end();
    mCamTop.draw("TOP");

    // CAM LEFT
    mCamLeft.begin();
    ofPushView();
    ofScale(mCamLeft.getScale());
    renderScene(mCamLeft, mCamLeft.getVisibleMesh());
    ofPopView();
    mCamLeft.end();
    mCamLeft.draw("LEFT");

    // CAM FRONT
    mCamFront.begin();
    ofPushView();
    ofScale(mCamFront.getScale());
    renderScene(mCamFront, mCamFront.getVisibleMesh());
    ofPopView();
    mCamFront.end();
    mCamFront.draw("FRONT");

    // CAM DEPHMAP
    //ofSetupScreenOrtho(2, 2, 0,1000) ;
    mCamDephMap.begin();
    ofPushView();
    ofScale(mCamDephMap.getScale());
    renderScene(mCamDephMap, mCamDephMap.getVisibleMesh());
    ofPopView();
    mCamDephMap.end();
    mCamDephMap.draw("DEPH MAP");

    /*
    mCamOrtho.begin();
    renderScene(mCamDephMap, mCamDephMap.getVisibleMesh());
    mCamOrtho.end();
    */

    // disable depth test
    ofDisableDepthTest();

    //ofSetupScreenOrtho(ofGetWidth(),ofGetHeight(),-1000,1000);
    // blur draw
    mBlur.draw();

    for(auto& perception : mPerceptions) {
        perception->drawInfos();
    }

    // GUI
    if(mBGui)
        drawGui();
}


void ofApp::loadXml() {
    mXml.loadFile("app.xml");

    mPointSize = mXml.getValue("settings:render:pointsize", 5.f);
    mRangeMin = mXml.getValue("settings:render::rangemin", 0.f);

    mNearDephtMap = mXml.getValue("settings:render::neardepthmap", 0.f);
    mFarDephtMap = mXml.getValue("settings:render::fardepthmap", 1000.f);
    mOrthoScale = mXml.getValue("settings:render::orthoscale", 10.f);

    mPositionRenderWin.x = mXml.getValue("settings:render:positionRenderWinX", 0.f);
    mPositionRenderWin.y = mXml.getValue("settings:render:positionRenderWinY", 0.f);
    mScaleRenderWin.x = mXml.getValue("settings:render:scaleRenderWinX", 1.f);
    mScaleRenderWin.y = mXml.getValue("settings:render:scaleRenderWinY", 1.f);

    mShiftRenderWin.x = mXml.getValue("settings:render:shiftRenderWinX", 0.f);
    mShiftRenderWin.y = mXml.getValue("settings:render:shiftRenderWinY", 0.f);
    mShiftRenderWin.z = mXml.getValue("settings:render:shiftRenderWinZ", 1.f);
    mShiftRenderWin.w = mXml.getValue("settings:render:shiftRenderWinW", 1.f);

}

void ofApp::deleteLandmark(unsigned int &id) {
     ofLandmark::deleteLandmark(mLandmarks, id);
     saveObjetsXml();
}

void ofApp::deletePerception(unsigned int &id) {
     ofPerception::deletePerception(mPerceptions, id);
     saveObjetsXml();
}

void ofApp::deleteArchi(unsigned int &id) {
     ofArchi::deleteArchi(mArchis, id);
     saveObjetsXml();
}

void ofApp::loadObjetsXml() {
    mObjetsXml.loadFile("objets.xml");

    // landmarks
    {
        mObjetsXml.pushTag("landmarks");
        int number = mObjetsXml.getNumTags("landmark");
        for(int i = 0; i < number; i++) {
            mObjetsXml.pushTag("landmark", i);

            int id = mObjetsXml.getValue("id", 0);
            ofLandmark* landmark = new ofLandmark(id);
            mLandmarks.push_back(landmark);
            ofAddListener(landmark->eDel, this, &ofApp::deleteLandmark);

            mObjetsXml.popTag();
        }
        mObjetsXml.popTag();
    }

    // perceptions
    {
        mObjetsXml.pushTag("perceptions");
        int number = mObjetsXml.getNumTags("perception");
        for(int i = 0; i < number; i++) {
            mObjetsXml.pushTag("perception", i);

            int id = mObjetsXml.getValue("id", 0);
            ofPerception* perception = new ofPerception(id);
        #ifdef ZILHOUETTE_ROS
            perception->setup(mNh);
        #else
            perception->setup();
        #endif
            mPerceptions.push_back(perception);
            ofAddListener(perception->eDel, this, &ofApp::deletePerception);

            mObjetsXml.popTag();
        }
        mObjetsXml.popTag();
    }

    // architecture
    {
        mObjetsXml.pushTag("archis");
        int number = mObjetsXml.getNumTags("archi");
        for(int i = 0; i < number; i++) {
            mObjetsXml.pushTag("archi", i);

            int id = mObjetsXml.getValue("id", 0);
            ofArchi* archi = new ofArchi(id);
            archi->setup();
            mArchis.push_back(archi);
            ofAddListener(archi->eDel, this, &ofApp::deleteArchi);

            mObjetsXml.popTag();
        }
        mObjetsXml.popTag();
    }
}

void ofApp::saveXml() {
    mXml.setValue("settings:render:pointsize", mPointSize);
    mXml.setValue("settings:render::rangefactor", mRangeFactor);
    mXml.setValue("settings:render::rangemin", mRangeMin);
    mXml.setValue("settings:render::neardepthmap", mNearDephtMap);
    mXml.setValue("settings:render::fardepthmap", mFarDephtMap);
    mXml.setValue("settings:render::orthoscale", mOrthoScale);

    mXml.setValue("settings:render:positionRenderWinX", mPositionRenderWin.x);
    mXml.setValue("settings:render:positionRenderWinY", mPositionRenderWin.y);
    mXml.setValue("settings:render:scaleRenderWinX", mScaleRenderWin.x);
    mXml.setValue("settings:render:scaleRenderWinY", mScaleRenderWin.y);

    mXml.setValue("settings:render:shiftRenderWinX", mShiftRenderWin.x);
    mXml.setValue("settings:render:shiftRenderWinY", mShiftRenderWin.y);
    mXml.setValue("settings:render:shiftRenderWinZ", mShiftRenderWin.z);
    mXml.setValue("settings:render:shiftRenderWinW", mShiftRenderWin.w);

    mXml.save("app.xml");
}

void ofApp::saveObjetsXml() {
    ofxXmlSettings settings;
    settings.addTag("landmarks");
    settings.pushTag("landmarks");
    for(int i = 0; i < mLandmarks.size(); i++){
        settings.addTag("landmark");
        settings.pushTag("landmark",i);
        settings.addValue("id", (int)mLandmarks[i]->getUniqueID());
        settings.popTag();
    }
    settings.popTag();

    settings.addTag("perceptions");
    settings.pushTag("perceptions");
    for(int i = 0; i < mPerceptions.size(); i++){
        settings.addTag("perception");
        settings.pushTag("perception",i);
        settings.addValue("id", (int)mPerceptions[i]->getUniqueID());
        settings.popTag();
    }
    settings.popTag();

    settings.addTag("archis");
    settings.pushTag("archis");
    for(int i = 0; i < mArchis.size(); i++){
        settings.addTag("archi");
        settings.pushTag("archi",i);
        settings.addValue("id", (int)mArchis[i]->getUniqueID());
        settings.popTag();
    }
    settings.popTag();

    settings.saveFile("objets.xml");
}

//-----------------------------------------------------------------------------
// [SECTION] Example App: Main Menu Bar / ShowExampleAppMainMenuBar()
//-----------------------------------------------------------------------------

void ofApp::drawGui() {

    //required to call this at beginning
    mGui.begin();

    appMainMenuBar();
    if(show_app_log)
        ShowExampleAppLog(&show_app_log);

    ImGui::SetNextWindowPos(ImVec2(100, 100), ImGuiCond_FirstUseEver);

    if (!ImGui::Begin("Render window", p_open_render))
    {
        ImGui::End();
        return;
    }

    if(ImGui::Button("Load")) {
        loadXml();
    }
    ImGui::SameLine();
    if(ImGui::Button("Save")) {
        saveXml();
    }

    if (ImGui::CollapsingHeader("POINT CLOUD")) {
        const float stepf = 10.f;

        ImGui::SliderFloat("Point size", &mPointSize, 0.0f, 50.f);
        ImGui::InputScalar("Range factor", ImGuiDataType_Float, &mRangeFactor, &stepf); //0.01f, 1000.f);
        ImGui::InputScalar("Range min", ImGuiDataType_Float, &mRangeMin, &stepf); //-1000.f, 10000.f);

        if(ImGui::InputScalar("Near depht", ImGuiDataType_Float, &mNearDephtMap, &stepf)) { // -100.f, 10000.f)) {
            mCamDephMap.mCam.setNearClip(mNearDephtMap);
        }

        if (ImGui::InputScalar("Far depht", ImGuiDataType_Float, &mFarDephtMap, &stepf)) { //0.f, 10000.f)) {
            mCamDephMap.mCam.setFarClip(mFarDephtMap);
        }

        ImGui::InputScalar("Ortho scale", ImGuiDataType_Float, &mOrthoScale, &stepf);

        if (ImGui::Button("Toggle fullscreen")) {
            ofToggleFullscreen();
        }
    }

    if (ImGui::CollapsingHeader("GRID")) {

        ImGui::Checkbox("Toggle draw grid : ", &mDrawGrid);
        ImGui::SetNextWindowPos(ImVec2(100, 100), ImGuiCond_FirstUseEver);
        ImGui::SliderFloat("Yaw Grid", &mAngleGrid.x, -360.f, 360.f);
        ImGui::SliderFloat("Pitch Grid", &mAngleGrid.y, -360.f, 360.f);
        ImGui::SliderFloat("Roll Grid", &mAngleGrid.z, -360.f, 360.f);
        ImGui::SliderFloat("Pos X Grid", &mPositionGrid.x, -10.0f, 10.f);
        ImGui::SliderFloat("Pos Y Grid", &mPositionGrid.y, -10.0f, 10.f);
        ImGui::SliderFloat("Pos z Grid", &mPositionGrid.z, -10.0f, 10.f);
    }

    // render window
    if (ImGui::CollapsingHeader("RENDER Windows")) {
        ImGui::Separator();
        ImGui::Checkbox("Visible", &mVisibleRenderWin);
        const float stepf_ = 0.01f;
        ImGui::InputScalar("Stepf", ImGuiDataType_Float, &mStepfRenderWin, &stepf_);
        ImGui::InputScalar("Pos X", ImGuiDataType_Float, &mPositionRenderWin.x, &mStepfRenderWin);
        ImGui::InputScalar("Pos Y", ImGuiDataType_Float, &mPositionRenderWin.y, &mStepfRenderWin);
        ImGui::InputScalar("Scale X", ImGuiDataType_Float, &mScaleRenderWin.x, &mStepfRenderWin);
        ImGui::InputScalar("Scale Y", ImGuiDataType_Float, &mScaleRenderWin.y, &mStepfRenderWin);
        ImGui::InputScalar("Shift X", ImGuiDataType_Float, &mShiftRenderWin.x, &mStepfRenderWin);
        ImGui::InputScalar("Shift Y", ImGuiDataType_Float, &mShiftRenderWin.y, &mStepfRenderWin);
        ImGui::InputScalar("Shift Z", ImGuiDataType_Float, &mShiftRenderWin.z, &mStepfRenderWin);
        ImGui::InputScalar("Shift W", ImGuiDataType_Float, &mShiftRenderWin.w, &mStepfRenderWin);
    }
    ImGui::End();

    // perception
    if(show_perceptions) {
        for(auto& perception : mPerceptions) {
            perception->drawGui();
        }
    }

    // landmarks
    if(show_landmarks) {
        for (auto& land : mLandmarks) {
            land->drawGui();
        }
    }

    // Architecture
    if(show_archis) {
        for(auto&  archi : mArchis) {
            archi->drawGui();
        }
    }
    // blur
    mBlur.drawGui();

    // cam
    mCamPers.drawGui();
    mCamTop.drawGui();
    mCamLeft.drawGui();
    mCamFront.drawGui();
    mCamDephMap.drawGui();

    //required to call this at end
    mGui.end();
}

// Demonstrate creating a fullscreen menu bar and populating it.
void ofApp::appMainMenuBar()
{
    if (ImGui::BeginMainMenuBar())
    {
        if (ImGui::BeginMenu("File"))
        {
            showMenuFile();
            ImGui::EndMenu();
        }
        if (ImGui::BeginMenu("Edit"))
        {
            if (ImGui::MenuItem("Undo", "CTRL+Z")) {}
            if (ImGui::MenuItem("Redo", "CTRL+Y", false, false)) {}  // Disabled item
            ImGui::Separator();
            if (ImGui::MenuItem("Cut", "CTRL+X")) {}
            if (ImGui::MenuItem("Copy", "CTRL+C")) {}
            if (ImGui::MenuItem("Paste", "CTRL+V")) {}
            ImGui::EndMenu();
        }
        if (ImGui::BeginMenu("Show"))
        {
            if (ImGui::MenuItem("Show log", "CTRL+L")) {
                show_app_log = true;
            }
            if (ImGui::MenuItem("Show landmarks")) {
                show_landmarks = !show_landmarks;
                for (auto& land : mLandmarks) {
                    land->setOpen(show_landmarks);
                }
            }
            if (ImGui::MenuItem("Show perceptions")) {
                show_perceptions = !show_perceptions;
                for (auto& per : mPerceptions) {
                    per->setOpen(show_perceptions);
                }
            }
            if (ImGui::MenuItem("Show archi")) {
                show_archis = !show_archis;
                for (auto& arch : mArchis) {
                    arch->setOpen(show_archis);
                }
            }
            ImGui::EndMenu();
        }
        if (ImGui::BeginMenu("Objet"))
        {
            if (ImGui::MenuItem("Add landmark", "CTRL+M")) {
                ofLandmark* landmark = new ofLandmark(ofLandmark::getMaxUniqueId()+1);
                mLandmarks.push_back(landmark);
                ofAddListener(landmark->eDel, this, &ofApp::deleteLandmark);
                saveObjetsXml();
            }

            if (ImGui::MenuItem("Add perception", "CTRL+P")) {
                ofPerception* perception = new ofPerception(ofPerception::getMaxUniqueId()+1);
                #ifdef ZILHOUETTE_ROS
                    perception->setup(mNh);
                #else
                    perception->setup();
                #endif
                mPerceptions.push_back(perception);
                ofAddListener(perception->eDel, this, &ofApp::deleteLandmark);
                saveObjetsXml();
            }

            if (ImGui::MenuItem("Add archi", "CTRL+A")) {
                ofArchi* archi = new ofArchi(ofArchi::getMaxUniqueId()+1);
                mArchis.push_back(archi);
                ofAddListener(archi->eDel, this, &ofApp::deleteArchi);
                saveObjetsXml();
            }

            ImGui::EndMenu();
        }
        ImGui::EndMainMenuBar();
    }
}

void ofApp::showMenuFile()
{
    ImGui::MenuItem("(dummy menu)", NULL, false, false);
    if (ImGui::MenuItem("New")) {}
    if (ImGui::MenuItem("Open", "Ctrl+O")) {}
    if (ImGui::BeginMenu("Open Recent"))
    {
        ImGui::MenuItem("fish_hat.c");
        ImGui::MenuItem("fish_hat.inl");
        ImGui::MenuItem("fish_hat.h");
        ImGui::EndMenu();
    }
    if (ImGui::MenuItem("Save", "Ctrl+S")) {}
    if (ImGui::MenuItem("Save As..")) {}
    ImGui::Separator();

    if (ImGui::MenuItem("Quit", "Alt+F4")) {}
}


//-----------------------------------------------------------------------------
// [SECTION] Example App: Debug Log / ShowExampleAppLog()
//-----------------------------------------------------------------------------

// Usage:
//  static ExampleAppLog my_log;
//  my_log.AddLog("Hello %d world\n", 123);
//  my_log.Draw("title");
struct ExampleAppLog
{
    ImGuiTextBuffer     Buf;
    ImGuiTextFilter     Filter;
    ImVector<int>       LineOffsets;        // Index to lines offset
    bool                ScrollToBottom;

    void    Clear()     { Buf.clear(); LineOffsets.clear(); }

    void    AddLog(const char* fmt, ...) IM_FMTARGS(2)
    {
        int old_size = Buf.size();
        va_list args;
        va_start(args, fmt);
        Buf.appendfv(fmt, args);
        va_end(args);
        for (int new_size = Buf.size(); old_size < new_size; old_size++)
            if (Buf[old_size] == '\n')
                LineOffsets.push_back(old_size);
        ScrollToBottom = true;
    }

    void    Draw(const char* title, bool* p_open = NULL)
    {
        ImGui::SetNextWindowSize(ImVec2(500,400), ImGuiCond_FirstUseEver);
        if (!ImGui::Begin(title, p_open))
        {
            ImGui::End();
            return;
        }
        if (ImGui::Button("Clear")) Clear();
        ImGui::SameLine();
        bool copy = ImGui::Button("Copy");
        ImGui::SameLine();
        Filter.Draw("Filter", -100.0f);
        ImGui::Separator();
        ImGui::BeginChild("scrolling", ImVec2(0,0), false, ImGuiWindowFlags_HorizontalScrollbar);
        if (copy) ImGui::LogToClipboard();

        if (Filter.IsActive())
        {
            //const char* buf_begin = Buf.begin();
            const char* buf_begin = ofApp::sChannel->getBuffer().begin();
            const char* line = buf_begin;
            for (int line_no = 0; line != NULL; line_no++)
            {
                const char* line_end = (line_no < LineOffsets.Size) ? buf_begin + LineOffsets[line_no] : NULL;
                if (Filter.PassFilter(line, line_end))
                    ImGui::TextUnformatted(line, line_end);
                line = line_end && line_end[1] ? line_end + 1 : NULL;
            }
        }
        else
        {
            //ImGui::TextUnformatted(Buf.begin());
            ImGui::TextUnformatted(ofApp::sChannel->getBuffer().begin());
        }

        if (ScrollToBottom)
            ImGui::SetScrollHere(1.0f);
        ScrollToBottom = false;
        ImGui::EndChild();
        ImGui::End();
    }
};

// Demonstrate creating a simple log window with basic filtering.
void ofApp::ShowExampleAppLog(bool* p_open)
{
    static ExampleAppLog log;

    // Demo: add random items (unless Ctrl is held)
    static double last_time = -1.0;
    double time = ImGui::GetTime();
    if (time - last_time >= 0.20f && !ImGui::GetIO().KeyCtrl)
    {
        //const char* random_words[] = { "system", "info", "warning", "error", "fatal", "notice", "log" };
        //log.AddLog("[%s] Hello, time is %.1f, frame count is %d\n", random_words[rand() % IM_ARRAYSIZE(random_words)], time, ImGui::GetFrameCount());
        last_time = time;
    }

    log.Draw("Example: Log", p_open);
}


//--------------------------------------------------------------
void ofApp::keyPressed(int key){
    switch(key) {
      case ' ':
            mBGui = !mBGui;
            //mCamOrtho.getOrtho() ? mCamOrtho.disableOrtho() : mCamOrtho.enableOrtho();
            break;
        case 'C':
        case 'c':
            //mCamOrtho.getMouseInputEnabled() ? mCamOrtho.disableMouseInput() : mCamOrtho.enableMouseInput();
            break;
        case 'F':
        case 'f':
            ofToggleFullscreen();
            break;
        case 'I':
        case 'i':
            //mCamOrtho.getInertiaEnabled() ? mCamOrtho.disableInertia() : mCamOrtho.enableInertia();
            break;
        case 'Y':
        case 'y':
            //mCamOrtho.setRelativeYAxis(!mCamOrtho.getRelativeYAxis());
            break;
    }
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){
    ofCam::resizePers(mCamPers);
    ofCam::resizeTop(mCamTop);
    ofCam::resizeLeft(mCamLeft);
    ofCam::resizeFront(mCamFront);
    ofCam::resizeDephMap(mCamDephMap);
}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}
