#include "ofMain.h"
#include "ofApp.h"

#include "zilhouette.h"

#ifdef ZILHOUETTE_ROS
	#include "ros/ros.h"
#endif

//========================================================================
int main( ){
    int argc = 0;
#ifdef ZILHOUETTE_ROS
    // roscore
    system("roscore &");
    // ros
    ros::init(argc, NULL, "Niform");
#endif

	#ifdef ZILHOUETTE_PROD4K
		glm::ivec2 constrol_size(1280.f * 2.f, 768.f * 2.f) ;
        glm::ivec2 render_size(RENDER_WINDOW_SIZE_X, RENDER_WINDOW_SIZE_Y);
		glm::vec2  render_position(3840.f, 0.f);
		bool	   bfullscreen(true);
	#else
        glm::ivec2 constrol_size(1920.f, 1080.f);
        glm::ivec2 render_size(RENDER_WINDOW_SIZE_X, RENDER_WINDOW_SIZE_Y);
		glm::vec2  render_position(1920.f, 0.f);
        bool	   bfullscreen(true);
	#endif

    // control window
    ofGLFWWindowSettings settings_control;
    settings_control.setGLVersion(4,3);
    settings_control.setSize(constrol_size.x, constrol_size.y);
    settings_control.title = "Niform CONTROL";
    shared_ptr<ofAppBaseWindow> controlWindow = ofCreateWindow(settings_control);

    // render window
    ofGLFWWindowSettings settings_render;
    settings_render.setGLVersion(4,3);
    settings_render.setSize(render_size.x, render_size.y);
    settings_render.setPosition(render_position);
    settings_render.title = "Niform RENDER";
    settings_render.shareContextWith = controlWindow;
    settings_render.decorated = false;
    shared_ptr<ofAppBaseWindow> renderWindow = ofCreateWindow(settings_render);
    renderWindow->setFullscreen(bfullscreen);

    shared_ptr<ofApp> mainApp(new ofApp);
    mainApp->setupRender();
    ofAddListener(renderWindow->events().draw,mainApp.get(),&ofApp::drawRender);

    ofRunApp(controlWindow, mainApp);
    ofRunMainLoop();
}

