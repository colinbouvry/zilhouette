#pragma once

#include "ofMain.h"

#include "ofxImGui.h"
#include "ofxXmlSettings.h"

#include "objet.h"

class ofLandmark : public Objet
{
public:
    ofLandmark();
    ofLandmark(unsigned int id);
    ~ofLandmark();
    static void deleteLandmark(vector<ofLandmark*>& landmarks, unsigned int &id);
    void reset();
    void setup();
    void draw();
    void drawGui();
    void loadXml();
    void saveXml();
    unsigned int            getUniqueID() { return sUniqueId; }
    static unsigned int     getMaxUniqueId() { return sMaxUniqueId; }
    void                    setOpen(bool open) { *p_open = open; }
    ofEvent<unsigned int>   eDel;
private:
    glm::vec3               mPosition;
    bool                    mVisible;
    ImVec4                  mColor;

    static unsigned int     sCount;
    unsigned int            sUniqueId;
    static unsigned int     sMaxUniqueId;
    bool                    *p_open;
    ofxXmlSettings          mXml;
    float                   mStepf;
};
