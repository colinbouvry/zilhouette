#include "ofArchi.h"

unsigned int     ofArchi::sCount = 0;
unsigned int     ofArchi::sMaxUniqueId = 0;

ofArchi::ofArchi()
    : Objet()
{
    reset();

    sCount++;
    sUniqueId = sCount;
    if(sUniqueId > sMaxUniqueId)
        sMaxUniqueId = sUniqueId;
    p_open = new bool(true);

    loadXml();
}

void ofArchi::deleteArchi(vector<ofArchi*>& archis, unsigned int &id) {

    auto it = archis.begin();
        while (it != archis.end())
        {
            // remove odd numbers
            if ((*it)->getUniqueID() == id ) {
                delete *it;
                // erase() invalidates the iterator, use returned iterator
                it = archis.erase(it);
            }
            // Notice that iterator is incremented only on the else part (why?)
            else {
                ++it;
            }
        }
}

ofArchi::ofArchi(unsigned int id)
    : Objet()
{
    reset();

    sCount++;
    sUniqueId = id;
    if(sUniqueId > sMaxUniqueId)
        sMaxUniqueId = sUniqueId;
    p_open = new bool(true);

    loadXml();
}

ofArchi::~ofArchi()
{

}

void ofArchi::setup()
{

}

void ofArchi::draw()
{
    if(!mVisible)
        return;

    // draw poteaux
    ofPushMatrix();
    ofFill();
    ofMatrix4x4 mat;
    mat.preMultTranslate(mPosition);
    mat.preMultRotate(ofQuaternion(mAngle.x, ofVec3f(1, 0, 0), mAngle.y, ofVec3f(0, 1, 0), mAngle.z, ofVec3f(0, 0, 1)));
    mat.preMultScale(mScale);
    mat.preMultTranslate(mPivot);
    ofMultMatrix(mat);
    ofSetColor(ofColor(mColor));
    ofDrawBox(1, 1, 1);
    ofPopMatrix();
}

void ofArchi::drawGui()
{
    if ((*p_open) == false)
        return;

    string sId = ofToString(sUniqueId);
    string title = "Archi Window " + sId;

    ImGui::SetNextWindowPos(ImVec2(100, 400.f + 800.f * sUniqueId), ImGuiCond_FirstUseEver);

    if (!ImGui::Begin(title.c_str(), p_open))
    {
        ImGui::End();
        return;
    }

    //if (ImGui::CollapsingHeader("Archis"))
    {
        if(ImGui::Button("Load")) {
            loadXml();
        }
        ImGui::SameLine();
        if(ImGui::Button("Save")) {
            saveXml();
        }
        ImGui::SameLine();
        if(ImGui::Button("Reset")) {
            reset();
        }
        ImGui::SameLine();
        if(ImGui::Button("Del")) {
            ofNotifyEvent(eDel, sUniqueId, this);
        }

        ImGui::Separator();
        ImGui::Checkbox("Visible", &mVisible);
        ImGui::ColorEdit4("Color", (float*)&mColor);
        ImGui::Separator();
        const float stepf = 0.01f;
        ImGui::InputScalar("Stepf", ImGuiDataType_Float, &mStepf, &stepf);
        ImGui::Separator();
        ImGui::InputScalar("Angle X", ImGuiDataType_Float, &mAngle.x, &mStepf);
        ImGui::InputScalar("Angle Y", ImGuiDataType_Float, &mAngle.y, &mStepf);
        ImGui::InputScalar("Angle Z", ImGuiDataType_Float, &mAngle.z, &mStepf);
        ImGui::InputScalar("Pos X", ImGuiDataType_Float, &mPosition.x, &mStepf);
        ImGui::InputScalar("Pos Y", ImGuiDataType_Float, &mPosition.y, &mStepf);
        ImGui::InputScalar("Pos z", ImGuiDataType_Float, &mPosition.z, &mStepf);
        ImGui::InputScalar("Scale X", ImGuiDataType_Float, &mScale.x, &mStepf);
        ImGui::InputScalar("Scale Y", ImGuiDataType_Float, &mScale.y, &mStepf);
        ImGui::InputScalar("Scale z", ImGuiDataType_Float, &mScale.z, &mStepf);
        ImGui::InputScalar("Pivot X", ImGuiDataType_Float, &mPivot.x, &mStepf);
        ImGui::InputScalar("Pivot Y", ImGuiDataType_Float, &mPivot.y, &mStepf);
        ImGui::InputScalar("Pivot z", ImGuiDataType_Float, &mPivot.z, &mStepf);
    }
    ImGui::End();
}

void ofArchi::reset()
{
    mPosition = glm::vec3(0.f, 0.25f, 0.f);
    mVisible = true;
    mStepf = 0.1f;
}

void ofArchi::loadXml() {
    mXml.loadFile("landmark_" + ofToString(sUniqueId) + ".xml");

    mStepf = mXml.getValue("settings:stepf", 0.1f);

    mPosition.x = mXml.getValue("settings:position:x", 0.0);
    mPosition.y = mXml.getValue("settings:position:y", 0.0);
    mPosition.z = mXml.getValue("settings:position:z", 0.0);

    mAngle.x = mXml.getValue("settings:angle:x", 0.0);
    mAngle.y = mXml.getValue("settings:angle:y", 0.0);
    mAngle.z = mXml.getValue("settings:angle:z", 0.0);

    mScale.x = mXml.getValue("settings:scale:x", 1.0);
    mScale.y = mXml.getValue("settings:scale:y", 1.0);
    mScale.z = mXml.getValue("settings:scale:z", 1.0);

    mPivot.x = mXml.getValue("settings:pivot:x", 0.0);
    mPivot.y = mXml.getValue("settings:pivot:y", 0.0);
    mPivot.z = mXml.getValue("settings:pivot:z", 0.0);

    mColor.x = mXml.getValue("settings:color:x", 0);
    mColor.y = mXml.getValue("settings:color:y", 0);
    mColor.z = mXml.getValue("settings:color:z", 0);
    mColor.w = mXml.getValue("settings:color:w", 255);

    mVisible = mXml.getValue("settings:visible", true);
}

void ofArchi::saveXml() {

    mXml.setValue("settings:stepf", mStepf);

    mXml.setValue("settings:position:x", mPosition.x);
    mXml.setValue("settings:position:y", mPosition.y);
    mXml.setValue("settings:position:z", mPosition.z);

    mXml.setValue("settings:angle:x", mAngle.x);
    mXml.setValue("settings:angle:y", mAngle.y);
    mXml.setValue("settings:angle:z", mAngle.z);

    mXml.setValue("settings:scale:x", mScale.x);
    mXml.setValue("settings:scale:y", mScale.y);
    mXml.setValue("settings:scale:z", mScale.z);

    mXml.setValue("settings:pivot:x", mPivot.x);
    mXml.setValue("settings:pivot:y", mPivot.y);
    mXml.setValue("settings:pivot:z", mPivot.z);

    mXml.setValue("settings:color:x", mColor.x);
    mXml.setValue("settings:color:y", mColor.y);
    mXml.setValue("settings:color:z", mColor.z);
    mXml.setValue("settings:color:w", mColor.w);

    mXml.save("landmark_" + ofToString(sUniqueId) + ".xml");
}
