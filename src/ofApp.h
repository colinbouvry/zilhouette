#pragma once

#include "ofMain.h"

#include "ofPerception.h"
#include "ofCam.h"
#include "ofBlur.h"
#include "ofLandmark.h"
#include "ofArchi.h"

#include "ofxImGui.h"
#include "ofxImGuiLoggerChannel.h"

#ifdef ZILHOUETTE_ROS
	#include "ros/ros.h"
	#include "sensor_msgs/PointCloud.h"
#endif

class ofApp : public ofBaseApp{

	public:
        ofApp();
        virtual ~ofApp();
		void setup();
        void setupRender();
        void drawRender(ofEventArgs & args);
		void update();
        void renderScene(const ofCam& cam, bool visible_depthmap = true);
        void renderSceneDephtMap( const ofCam& cam);
		void draw();
        void drawGui();
        void appMainMenuBar();
        static void showMenuFile();
        static void ShowExampleAppLog(bool* p_open);
		void keyPressed(int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y );
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void mouseEntered(int x, int y);
		void mouseExited(int x, int y);
		void windowResized(int w, int h);
		void dragEvent(ofDragInfo dragInfo);
		void gotMessage(ofMessage msg);
        void loadXml();
        void saveXml();
        void loadObjetsXml();
        void saveObjetsXml();
        void deleteLandmark(unsigned int& id);
        void deletePerception(unsigned int &id);
        void deleteArchi(unsigned int &id);
    private:
        // show
        static bool show_app_log;
        static bool show_landmarks;
        static bool show_perceptions;
        static bool show_archis;

#ifdef ZILHOUETTE_ROS
        // ROS
        ros::NodeHandle mNh;
#endif

        // mesh
        ofVboMesh               mesh;

        // perception
        vector<ofPerception*>   mPerceptions;
		
        // gui
        ofxImGui::Gui           mGui;
        float                   mPointSize;

        // render win GUI
        bool					mVisibleRenderWin;
        float					mStepfRenderWin;
        glm::vec2				mPositionRenderWin;
        glm::vec2				mScaleRenderWin;
        glm::vec4               mShiftRenderWin;

        // cam
        ofCam                   mCamPers;
        ofCam                   mCamTop;
        ofCam                   mCamLeft;
        ofCam                   mCamFront;
        ofCam                   mCamDephMap;

        // grid
        glm::vec3               mAngleGrid;
        glm::vec3               mPositionGrid;
        bool                    mDrawGrid;

        // blur
        ofBlur                  mBlur;
        ofShader                mShaderDepth;
        float                   mRangeFactor;
        float                   mRangeMin;
        float                   mAlphaArchi;

        // landmark
        std::vector<ofLandmark*> mLandmarks;

        // center screen
        glm::vec3 mCenterScreen;

        //
        float mOrthoScale;
        float mNearDephtMap;
        float mFarDephtMap;

        // archi
        vector<ofArchi*>         mArchis;

        //ofEasyCam mCamOrtho;

        // xml
        ofxXmlSettings          mXml;
        ofxXmlSettings          mObjetsXml;

        bool                    *p_open_render;

        bool                    mBGui;

public:
        static std::shared_ptr<ofxImGui::LoggerChannel> sChannel;
};
