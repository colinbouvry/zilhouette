#pragma once

#include "ofMain.h"

#include "zilhouette.h"
#include "objet.h"

#ifdef ZILHOUETTE_ROS
	#include "ros/ros.h"
	#include "sensor_msgs/PointCloud.h"
	#include "sensor_msgs/PointCloud2.h"
	#include "sensor_msgs/point_cloud2_iterator.h"
#endif

#include "ofxImGui.h"
#include "ofxXmlSettings.h"

class ofPerception : public Objet
{
public:
    ofPerception();
    ofPerception(unsigned int id);
    ~ofPerception();
    static void deletePerception(vector<ofPerception*>& perceptions, unsigned int &id);
    void reset();
#ifdef ZILHOUETTE_ROS
    void setup(ros::NodeHandle& handle/*, const string& ros_sub, int type = 1*/);
#else
	void setup();
#endif

    void update();
    void drawMesh();
    void drawInfos();
    void draw();
    void drawGui();
    ofFloatColor getColor() {
        return mColor;
    }
    bool getColorMode() { return mColorMode; }
    void loadXml();
    void saveXml();
    unsigned int            getUniqueID() { return sUniqueId; }
    static unsigned int     getMaxUniqueId() { return sMaxUniqueId; }
    void                    setOpen(bool open) { *p_open = open; }
    ofEvent<unsigned int>   eDel;
    void rosLaunch();
    void rosKill();
private:
#ifdef ZILHOUETTE_ROS
    void cloudCallback(const sensor_msgs::PointCloudConstPtr& msg);
    void cloud2Callback(const sensor_msgs::PointCloud2ConstPtr& msg);
#endif
	void computeMat();
#ifdef ZILHOUETTE_ROS
    ros::Subscriber         mPointCloudSub;
#endif
    ofVboMesh               mesh;
    ofConePrimitive*        mCamMesh;
    std::string             mRosSub;

    float                   mThresholdConfidence;
    glm::vec3               mAngle;
    glm::vec3               mPosition;
    glm::vec3               mScale;
    glm::vec3               mPivot;
    bool                    mVisible;
    ImVec4                  mColor;
    bool                    mColorMode;
    ofMatrix4x4             mMat;

    static unsigned int     sCount;
    unsigned int            sUniqueId;
    static unsigned int     sMaxUniqueId;
    bool                    *p_open;
    ofxXmlSettings          mXml;
    float                   mStepf;
    int                     mType;

#ifdef ZILHOUETTE_ROS
    ros::NodeHandle*        mHandle;
#endif

    float                   mPointCloudAverageFactor;

    glm::vec3               mMinFilter;
    glm::vec3               mMaxFilter;

    bool                    mActive;
    bool                    mLaunch;
    float                   mLastActiveSecond;
    float                   mLastKillSecond;
};

