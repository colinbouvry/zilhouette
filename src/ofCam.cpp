#include "ofCam.h"

#include "ofxImGui.h"

unsigned int     ofCam::sCount = 0;

ofCam::ofCam()
    : mControlCam(true)
    , mVisibleMesh(true)
    , mScale(1.f)
{
    sCount++;
    sUniqueId = sCount;
    p_open = new bool(true);
}

ofCam::~ofCam()
{

}

void ofCam::reset() {
    mPosition = mPositionReset;
    mTarget = mTargetReset;
    mStepf = 1.f;
    mRangeFactor = 25.f;
}

void ofCam::saveXml() {
    mXml.setValue("settings:position:x", mPosition.x);
    mXml.setValue("settings:position:y", mPosition.y);
    mXml.setValue("settings:position:z", mPosition.z);

    mXml.setValue("settings:target:x", mTarget.x);
    mXml.setValue("settings:target:y", mTarget.y);
    mXml.setValue("settings:target:z", mTarget.z);

    mXml.setValue("settings:scaleX", mScale.x);
    mXml.setValue("settings:scaleY", mScale.y);
    mXml.setValue("settings:scaleZ", mScale.z);

    mXml.setValue("settings:stepf", mStepf);
    mXml.setValue("settings:rangefactor", mRangeFactor);

    mXml.save("cam_" + ofToString(sUniqueId) + ".xml");

}

void ofCam::loadXml() {
    mXml.load("cam_" + ofToString(sUniqueId) + ".xml");
    mPosition = glm::vec3(
            mXml.getValue("settings:position:x", mPositionReset.x),
            mXml.getValue("settings:position:y", mPositionReset.y),
            mXml.getValue("settings:position:z", mPositionReset.z)
    );
    mTarget = glm::vec3(
            mXml.getValue("settings:target:x", mTargetReset.x),
            mXml.getValue("settings:target:y", mTargetReset.y),
            mXml.getValue("settings:target:z", mTargetReset.z)
    );

    mScale.x = mXml.getValue("settings:scaleX", 1.f);
    mScale.y = mXml.getValue("settings:scaleY", 1.f);
    mScale.z = mXml.getValue("settings:scaleZ", 1.f);
    mRangeFactor = mXml.getValue("settings:rangefactor", 25.0);
}

void ofCam::begin() {
    mCam.begin(mRectangle);    
}

void ofCam::beginNoRect() {
    mCam.begin();
}

void ofCam::begin(const ofRectangle& rect) {
    mCam.begin();
}

void ofCam::end() {
    mCam.end();
}

void ofCam::update(bool windowHovered) {
    // update cam
    if(mControlCam) {
        mPosition = mCam.getPosition() / mScale;
        mTarget = mCam.getTarget().getGlobalPosition() / mScale;
    } else {
       mCam.setPosition(mPosition* mScale);
       mCam.setTarget(mTarget* mScale);
    }

    if(windowHovered) {
        mCam.disableMouseInput();
        mCam.disableMouseMiddleButton();
    } else {
        mCam.enableMouseInput();
        mCam.enableMouseMiddleButton();
    }
}

void ofCam::draw(const string& text) {
    ofSetColor(ofColor(0, 0, 0, 255));
    ofNoFill();
    ofDrawRectangle(mRectangle);
    ofDrawBitmapString(text, mRectangle.x + 20.f, mRectangle.y + 20.f );
}

void ofCam::setupPers(ofCam& cam)
{
    cam.mName = "PERS";
    cam.mPositionReset = glm::vec3(0.f);
    cam.mTargetReset = glm::vec3(0.f, 0.f, 10.f);

    cam.reset();
    cam.loadXml();

    cam.mCam.setPosition(cam.mPosition);
    cam.mCam.setTarget(cam.mTarget);
    cam.mCam.setNearClip(0.0001f);
    //mCamPers.rotate(180, glm::vec3(0,0,1) );

    resizePers(cam);
}

void ofCam::resizePers(ofCam& cam) {
    cam.mRectangle = ofRectangle(0.f, 40.f, ofGetWidth() * 0.75f, ofGetHeight());
}

void ofCam::setupTop(ofCam& cam)
{
    cam.mName = "TOP";
    cam.mPositionReset = glm::vec3(0.f, -20.f, 0.f);
    cam.mTargetReset = glm::vec3(0.f);

    cam.reset();
    cam.loadXml();

    cam.mCam.setPosition(cam.mPosition);
    cam.mCam.setTarget(cam.mTarget);
    cam.mCam.setNearClip(0.0001f);
    //mCamTop.rotate(180, glm::vec3(0,0,1) );
    resizeTop(cam);
}

void ofCam::resizeTop(ofCam& cam) {
    cam.mRectangle = ofRectangle(ofGetWidth() * 0.75f, 40.f, ofGetWidth() * 0.25f, ofGetHeight()* 0.25f);
}

void ofCam::setupLeft(ofCam& cam)
{
    cam.mName = "LEFT";
    cam.mPositionReset = glm::vec3(-20.f, 0.f, 0.f);
    cam.mTargetReset = glm::vec3(0.f);

    cam.reset();
    cam.loadXml();

    cam.mCam.setPosition(cam.mPosition);
    cam.mCam.setTarget(cam.mTarget);
    cam.mCam.setNearClip(0.0001f);
    //mCamTop.rotate(180, glm::vec3(0,0,1) );
    resizeLeft(cam);
}

void ofCam::resizeLeft(ofCam& cam) {
    cam.mRectangle = ofRectangle(ofGetWidth() * 0.75f, 40.f + ofGetHeight()* 0.25f, ofGetWidth() * 0.25f, ofGetHeight()* 0.25f);
}

void ofCam::setupFront(ofCam& cam)
{
    cam.mName = "FRONT";
    cam.mPositionReset = glm::vec3(0.f, 0.f, -20.f);
    cam.mTargetReset = glm::vec3(0.f);

    cam.reset();
    cam.loadXml();

    cam.mCam.setPosition(cam.mPosition);
    cam.mCam.setTarget(cam.mTarget);
    cam.mCam.setNearClip(0.0001f);
    //mCamTop.rotate(180, glm::vec3(0,0,1) );
    resizeFront(cam);
}

void ofCam::resizeFront(ofCam& cam) {
    cam.mRectangle = ofRectangle(ofGetWidth() * 0.75f, 40.f + ofGetHeight()* 0.5f, ofGetWidth() * 0.25f, ofGetHeight()* 0.25f);
}

void ofCam::setupDephMap(ofCam& cam)
{
    cam.mName = "DEPHTMAP";
    cam.mCam.enableOrtho(); // TO DO ERROR;

    cam.mScale = glm::vec3(100.);
    cam.mPositionReset = glm::vec3(0.f);
    cam.mTargetReset = glm::vec3(0.f, 0.f, 20.f);

    cam.reset();
    cam.loadXml();

    //cam.mCam.setFov(1.f); // TO DO LIKE ORTHO
    cam.mCam.setPosition(cam.mPosition * cam.mScale);
    cam.mCam.setTarget(cam.mTarget * cam.mScale);
    //cam.mCam.setNearClip(0.0001f);
    //cam.mCam.setFarClip(10000.f);
    //mCamTop.rotate(180, glm::vec3(0,0,1) );

     resizeDephMap(cam);
}

void ofCam::setupDephMapOrtho(ofCam& cam)
{
    /*
    cam.mName = "DEPHTMAP";
    cam.mCamOrtho.enableOrtho(); // TO DO ERROR

    //mPositionReset = glm::vec3(0.f);
    cam.mTargetReset = glm::vec3(0.f, 0.f, 20.f);

    cam.reset();
    cam.loadXml();
    */
}

void ofCam::resizeDephMap(ofCam& cam) {
    cam.mRectangle = ofRectangle(ofGetWidth() * 0.75f, 40.f + ofGetHeight()* 0.75f, ofGetWidth() * 0.25f, ofGetHeight()* 0.25f);
    //cam.mCam.setControlArea(cam.mRectangle);
    //cam.mRectangle.scaleFromCenter(0.1f); // ORTHO
}


void ofCam::drawGui()
{
    string title = "Camera Window " + mName;

    ImGui::SetNextWindowPos(ImVec2(100, 400.f), ImGuiCond_FirstUseEver);
    if (!ImGui::Begin(title.c_str(), p_open))
    {
        ImGui::End();
        return;
    }

    ImGui::Separator();
    ImGui::Text("General :");

    if(ImGui::Button("Load")) {
        loadXml();
        mCam.setPosition(mPosition * mScale);
        mCam.setTarget(mTarget* mScale);
    }
    ImGui::SameLine();
    if(ImGui::Button("Save")) {
        saveXml();
    }
    ImGui::SameLine();
    if(ImGui::Button("Reset")) {
        reset();
        mCam.setPosition(mPositionReset* mScale);
        mCam.setTarget(mTargetReset* mScale);
    }

    ImGui::Separator();
    ImGui::Checkbox("Visible meshs", &mVisibleMesh);
    ImGui::Separator();
    const float stepf = 1.f;
    ImGui::InputScalar("Range factor", ImGuiDataType_Float, &mRangeFactor, &stepf);
    ImGui::Separator();
    ImGui::Text("Transform :");
    ImGui::InputScalar("Stepf", ImGuiDataType_Float, &mStepf, &stepf);
    ImGui::InputScalar("Pos X Cam",   ImGuiDataType_Float,  &mPosition.x, &mStepf);
    ImGui::InputScalar("Pos Y Cam",   ImGuiDataType_Float,  &mPosition.y, &mStepf);
    ImGui::InputScalar("Pos Z Cam",   ImGuiDataType_Float,  &mPosition.z, &mStepf);
    ImGui::InputScalar("Tar X Cam",   ImGuiDataType_Float,  &mTarget.x, &mStepf);
    ImGui::InputScalar("Tar Y Cam",   ImGuiDataType_Float,  &mTarget.y, &mStepf);
    ImGui::InputScalar("Tar Z Cam",   ImGuiDataType_Float,  &mTarget.z, &mStepf);
    ImGui::InputScalar("Scale X", ImGuiDataType_Float, &mScale.x, &stepf);
    ImGui::InputScalar("Scale Y", ImGuiDataType_Float, &mScale.y, &stepf);
    ImGui::InputScalar("Scale Z", ImGuiDataType_Float, &mScale.z, &stepf);

    ImGui::Checkbox("Toggle control cam", &mControlCam);

    ImGui::End();
}
