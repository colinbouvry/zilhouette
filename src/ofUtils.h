#pragma once

#include "ofMain.h"
// https://stackoverflow.com/questions/785097/how-do-i-implement-a-b%C3%A9zier-curve-in-c
// https://cubic-bezier.com/
glm::vec2 getBezierPoint( glm::vec2* points, int numPoints, float t ) {
    glm::vec2* tmp = new glm::vec2[numPoints];
    memcpy(tmp, points, numPoints * sizeof(glm::vec2));
    int i = numPoints - 1;
    while (i > 0) {
        for (int k = 0; k < i; k++)
            tmp[k] = tmp[k] + t * ( tmp[k+1] - tmp[k] );
        i--;
    }
    glm::vec2 answer = tmp[0];
    delete[] tmp;
    return answer;
}
