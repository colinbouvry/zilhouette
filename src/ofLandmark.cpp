#include "ofLandmark.h"

unsigned int     ofLandmark::sCount = 0;
unsigned int     ofLandmark::sMaxUniqueId = 0;

ofLandmark::ofLandmark()
    : Objet()
{
    reset();

    sCount++;
    sUniqueId = sCount;
    if(sUniqueId > sMaxUniqueId)
        sMaxUniqueId = sUniqueId;
    p_open = new bool(true);

    loadXml();
}

void ofLandmark::deleteLandmark(vector<ofLandmark*>& landmarks, unsigned int &id) {

    auto it = landmarks.begin();
        while (it != landmarks.end())
        {
            // remove odd numbers
            if ((*it)->getUniqueID() == id ) {
                delete *it;
                // erase() invalidates the iterator, use returned iterator
                it = landmarks.erase(it);
            }
            // Notice that iterator is incremented only on the else part (why?)
            else {
                ++it;
            }
        }
}

ofLandmark::ofLandmark(unsigned int id)
    : Objet()
{
    reset();

    sCount++;
    sUniqueId = id;
    if(sUniqueId > sMaxUniqueId)
        sMaxUniqueId = sUniqueId;
    p_open = new bool(true);

    loadXml();
}

ofLandmark::~ofLandmark()
{

}

void ofLandmark::reset()
{
    mPosition = glm::vec3(0.f, 0.25f, 0.f);
    mVisible = true;
    mStepf = 0.1f;
    mColor = ImColor(114.f/255.f, 144.f/255.f, 154.f/255.f, 200.f/255.f);
}

void ofLandmark::loadXml() {
    mXml.loadFile("landmark_" + ofToString(sUniqueId) + ".xml");

    mStepf = mXml.getValue("settings:stepf", 0.1f);

    mPosition.x = mXml.getValue("settings:position:x", 0.0);
    mPosition.y = mXml.getValue("settings:position:y", 0.0);
    mPosition.z = mXml.getValue("settings:position:z", 0.0);

    mColor.x = mXml.getValue("settings:color:x", 114.f/255.f);
    mColor.y = mXml.getValue("settings:color:y", 144.f/255.f);
    mColor.z = mXml.getValue("settings:color:z", 154.f/255.f);
    mColor.w = mXml.getValue("settings:color:w", 200.f/255.f);
    mVisible = mXml.getValue("settings:visible", true);
}

void ofLandmark::saveXml() {

    mXml.setValue("settings:stepf", mStepf);

    mXml.setValue("settings:position:x", mPosition.x);
    mXml.setValue("settings:position:y", mPosition.y);
    mXml.setValue("settings:position:z", mPosition.z);

    mXml.setValue("settings:color:x", mColor.x);
    mXml.setValue("settings:color:y", mColor.y);
    mXml.setValue("settings:color:z", mColor.z);
    mXml.setValue("settings:color:w", mColor.w);
    mXml.setValue("settings:visible", mVisible);

    mXml.save("landmark_" + ofToString(sUniqueId) + ".xml");
}

void ofLandmark::setup()
{

}

void ofLandmark::draw()
{
    if(!mVisible)
        return;

    ofPushMatrix();
    ofTranslate(mPosition);
    ofSetColor(ofColor(mColor));
    ofFill();
    ofScale(glm::vec3(1.f,-1.f, 1.f));
    ofDrawCone(0.09f, 0.5f);
    ofPopMatrix();
}

void ofLandmark::drawGui()
{
    if ((*p_open) == false)
        return;

    string sId = ofToString(sUniqueId);
    string title = "Landmark Window " + sId;

    ImGui::SetNextWindowPos(ImVec2(100, 400.f + 800.f * sUniqueId), ImGuiCond_FirstUseEver);
    if (!ImGui::Begin(title.c_str(), p_open))
    {
        ImGui::End();
        return;
    }

    if(ImGui::Button("Load")) {
        loadXml();
    }
    ImGui::SameLine();
    if(ImGui::Button("Save")) {
        saveXml();
    }
    ImGui::SameLine();
    if(ImGui::Button("Reset")) {
        reset();
    }
    ImGui::SameLine();
    if(ImGui::Button("Del")) {
        ofNotifyEvent(eDel, sUniqueId, this);
    }

    ImGui::Separator();
    ImGui::Checkbox("Visible", &mVisible);
    ImGui::ColorEdit4("Color", (float*)&mColor);
    ImGui::Separator();
    const float stepf = 0.01f;
    ImGui::InputScalar("Stepf", ImGuiDataType_Float, &mStepf, &stepf);
    ImGui::InputScalar("Pos X", ImGuiDataType_Float, &mPosition.x, &mStepf);
    ImGui::InputScalar("Pos Y", ImGuiDataType_Float, &mPosition.y, &mStepf);
    ImGui::InputScalar("Pos z", ImGuiDataType_Float, &mPosition.z, &mStepf);

    ImGui::End();
}
