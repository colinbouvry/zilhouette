#pragma once

#include "ofMain.h"

#include "ofxImGui.h"
#include "ofxXmlSettings.h"

#include "objet.h"

class ofArchi : public Objet
{
public:
    ofArchi();
    ofArchi(unsigned int id);
    ~ofArchi();
    static void deleteArchi(vector<ofArchi*>& archis, unsigned int &id);
    void reset();
    void setup();
    void draw();
    void drawGui();
    void loadXml();
    void saveXml();
    unsigned int            getUniqueID() { return sUniqueId; }
    static unsigned int     getMaxUniqueId() { return sMaxUniqueId; }
    void                    setOpen(bool open) { *p_open = open; }
    ofEvent<unsigned int>   eDel;
private:
    glm::vec3               mPosition;
    glm::vec3               mAngle;
    glm::vec3               mScale;
    glm::vec3               mPivot;
    bool                    mVisible;
    ImVec4                  mColor;

    static unsigned int     sCount;
    unsigned int            sUniqueId;
    static unsigned int     sMaxUniqueId;
    bool                    *p_open;
    ofxXmlSettings          mXml;
    float                   mStepf;
};
