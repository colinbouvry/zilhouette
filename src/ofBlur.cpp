#include "ofBlur.h"

#include "ofxImGui.h"

//--------------------------------------------------------------
ofBlur::ofBlur()
   :  mSigma(20.f)
    , mMirrorOffsetY(-1.f)
    , mBlurFactor(50.f)
    , mSigmaFilter(20.f)
    , mBlurFilterFactor(10.f)
    , mErodeFilterFactor(0.f)
    , mDilateFilterFactor(0.f)
	, mScaleRender(1.f)
    , mSelectRender(5)
    , mSelectRenderResult(5)
    , mVisible(true)
    , mIndexMotionBlur(0)
   , mDephtSizeFactor(1.f)
{
    p_open = new bool(true);
    loadXml();
}

//--------------------------------------------------------------
ofBlur::~ofBlur()
{

}

//--------------------------------------------------------------
void ofBlur::setup()
{
#ifdef TEXTURE_BLUR
    mShaderTextureBlur.load("shadersGL3/shaderTextureBlur");
#else
    mShaderBlurX.load("shadersGL3/shaderBlurX");
    mShaderBlurY.load("shadersGL3/shaderBlurY");
#endif

    mImage.load("niform_ImageCRS_Tra11B3_Thessalonique_2021.png");

    mShaderFilterX.load("shadersGL3/shaderFilter2X");
    mShaderFilterY.load("shadersGL3/shaderFilter2Y");

    mShaderMotionBlur.load("shadersGL3/shaderMotionBlur");

    mShaderFilterDilEro.load("shadersGL3/shaderFilterDilEro");

    mShaderMirror.load("shadersGL3/shaderMirror");

    mSize = glm::vec2(mImage.getWidth(), mImage.getHeight());

#ifdef TEXTURE_BLUR
    mFboTextureBlur.allocate(mSize.x, mSize.y);

    for (int i = 0 ; i < sTextureBlurSize ; ++i) {
        string num = ofToString(i);
        if(num.size() == 1)
            num= "0" + num;
        ofLoadImage(mTextures[i], "niform_ImageCRS_Tra15_Thessalonique_2021/" + num + "_niform_ImageCRS_Tra15_Thessalonique_2021.png");
    }
#else
    mFboBlurOnePass.allocate(mSize.x, mSize.y);
    mFboBlurTwoPass.allocate(mSize.x, mSize.y);
#endif

    allocateFbos();
}

void ofBlur::allocateFbos() {

    glm::vec2 mSizeScaled = mDephtSizeFactor * glm::vec2(mSize);

    mFboMirror.allocate(mSizeScaled.x, mSizeScaled.y);

    mFboFilterDilEro.allocate(mSizeScaled.x, mSizeScaled.y);

    mFboFilterDilEro2.allocate(mSizeScaled.x, mSizeScaled.y);

    mFboFilterOnePass.allocate(mSizeScaled.x, mSizeScaled.y);
    mFboFilterTwoPass.allocate(mSizeScaled.x, mSizeScaled.y);

    mFboMotionBlur.allocate(mSizeScaled.x, mSizeScaled.y);
    for (int i = 0 ; i < 8 ; ++i) {
        mFboMotionBlurBuffer[i].allocate(mSizeScaled.x, mSizeScaled.y);
    }

    mFboDephMap.allocate(mSizeScaled.x, mSizeScaled.y);
}

//--------------------------------------------------------------
void ofBlur::dephBegin()
{
    mFboDephMap.begin();
    ofClear(ofColor(0,0,0,1));
}

//--------------------------------------------------------------
void ofBlur::dephEnd()
{
    mFboDephMap.end();
}

//--------------------------------------------------------------
void ofBlur::drawResult(const glm::vec2& scale, const glm::vec2& position)
{
    glm::vec2 s = mSize * scale;
    ofPushMatrix();
    ofTranslate(position);
    switch(mSelectRenderResult)
    {
        case 0:
            ofSetColor(ofColor::white);
            mImage.getTexture().draw(0, 0, s.x, s.y);
        break;

        case 1:
            ofSetColor(ofColor::white);
            mFboDephMap.getTexture().draw(0, 0, s.x, s.y);
        break;

        case 2:
            ofSetColor(ofColor::white);
            mFboMirror.getTexture().draw(0, 0, s.x, s.y);
        break;

        case 3:
            ofSetColor(ofColor::white);
            mFboFilterDilEro2.getTexture().draw(0, 0, s.x, s.y);
        break;

        case 4:
            ofSetColor(ofColor::white);
            mFboFilterTwoPass.getTexture().draw(0, 0, s.x, s.y);
        break;

        case 5:
            ofSetColor(ofColor::white);
            mFboMotionBlur.getTexture().draw(0, 0, s.x, s.y);
        break;

        case 6:
            ofSetColor(ofColor::white);
            mFboMotionBlurBuffer[0].getTexture().draw(0, 0, s.x, s.y);
        break;

        case 7:
            ofSetColor(ofColor::white);
            #ifdef TEXTURE_BLUR
            mFboTextureBlur.getTexture().draw(0, 0, s.x, s.y);
            #else
            mFboBlurTwoPass.getTexture().draw(0, 0, s.x, s.y);
            #endif
        break;
    }
    ofPopMatrix();
}

//--------------------------------------------------------------
void ofBlur::draw()
{
    if(!mVisible)
        return;

	glm::vec2 s = mSize * mScaleRender;

    ofDisableDepthTest();

    // FILTER MIRROR
    {
        ofFbo* inResult = &mFboDephMap;

       //----------------------------------------------------------
       mFboMirror.begin();

       mShaderMirror.begin();
       mShaderMirror.setUniformTexture("tex0", inResult->getTexture(), 0);
       mShaderMirror.setUniform1f("offsetY", mMirrorOffsetY);
       inResult->draw(0, 0);

       mShaderMirror.end();

       mFboMirror.end();

     }

    // FILTER ERODE
    {
        ofFbo* inResult = &mFboMirror;

       //----------------------------------------------------------
       mFboFilterDilEro.begin();

       mShaderFilterDilEro.begin();
       mShaderFilterDilEro.setUniformTexture("tex0", inResult->getTexture(), 0);
       mShaderFilterDilEro.setUniform1f("ublurAmnt", mErodeFilterFactor);
       mShaderFilterDilEro.setUniform1i("uDilate", 0);

       inResult->draw(0, 0);

       mShaderFilterDilEro.end();

       mFboFilterDilEro.end();

     }

    // FILTER DILATE
    {
        ofFbo* inResult = &mFboFilterDilEro;

       //----------------------------------------------------------
       mFboFilterDilEro2.begin();

       mShaderFilterDilEro.begin();
       mShaderFilterDilEro.setUniformTexture("tex0", inResult->getTexture(), 0);
       mShaderFilterDilEro.setUniform1f("ublurAmnt", mDilateFilterFactor);
       mShaderFilterDilEro.setUniform1i("uDilate", 1);

       inResult->draw(0, 0);

       mShaderFilterDilEro.end();

       mFboFilterDilEro2.end();

     }

     // FILTER DEPTHMAP
     {
        ofFbo* inResult = &mFboFilterDilEro2;

        //--------------------------------------------------------
        inResult->getTexture().bind(0);

        //----------------------------------------------------------
        mFboFilterOnePass.begin();

        mShaderFilterX.begin();
        mShaderFilterX.setUniformTexture("tex0", inResult->getTexture(), 0);
        mShaderFilterX.setUniform1f("ublurAmnt", mBlurFilterFactor);
        //mShaderFilterX.setUniform1f("uerodeAmnt", mErodeFilterFactor);
        //mShaderFilterX.setUniform1f("uerodeThres", mErodeThres);
        mShaderFilterX.setUniform1f("usigma", mSigmaFilter);

        inResult->draw(0, 0);

        mShaderFilterX.end();

        mFboFilterOnePass.end();

        //----------------------------------------------------------
        mFboFilterTwoPass.begin();

        mShaderFilterY.begin();
        mShaderFilterY.setUniformTexture("tex0", inResult->getTexture(), 0);
        mShaderFilterY.setUniform1f("ublurAmnt", mBlurFilterFactor);
        //mShaderFilterY.setUniform1f("uerodeAmnt", mErodeFilterFactor);
        //mShaderFilterY.setUniform1f("uerodeThres", mErodeThres);
        mShaderFilterY.setUniform1f("usigma", mSigmaFilter);

        // TO DO FIX BUG
        mFboFilterOnePass.getTexture().draw(0, 0);

        mShaderFilterY.end();

        mFboFilterTwoPass.end();
        //--------------------------------------------------------
        inResult->getTexture().unbind();
    }

    // MOTION BLUR
    {
        ofFbo* inResult = &mFboFilterTwoPass;

        mFboMotionBlurBuffer[mIndexMotionBlur].begin();
        ofSetColor(255);
        inResult->getTexture().draw(0, 0);

        mFboMotionBlurBuffer[mIndexMotionBlur].end();

        mFboMotionBlur.begin();

        mShaderMotionBlur.begin();

        if( mBlurSize > 0) mShaderMotionBlur.setUniformTexture("tex0", mFboMotionBlurBuffer[0].getTexture(), 0);
        if( mBlurSize > 1) mShaderMotionBlur.setUniformTexture("tex1", mFboMotionBlurBuffer[1].getTexture(), 1);
        if( mBlurSize > 2) mShaderMotionBlur.setUniformTexture("tex2", mFboMotionBlurBuffer[2].getTexture(), 2);
        if( mBlurSize > 3) mShaderMotionBlur.setUniformTexture("tex3", mFboMotionBlurBuffer[3].getTexture(), 3);
        if( mBlurSize > 4) mShaderMotionBlur.setUniformTexture("tex4", mFboMotionBlurBuffer[4].getTexture(), 4);
        if( mBlurSize > 5) mShaderMotionBlur.setUniformTexture("tex5", mFboMotionBlurBuffer[5].getTexture(), 5);
        if( mBlurSize > 6) mShaderMotionBlur.setUniformTexture("tex6", mFboMotionBlurBuffer[6].getTexture(), 6);
        if( mBlurSize > 7) mShaderMotionBlur.setUniformTexture("tex7", mFboMotionBlurBuffer[7].getTexture(), 7);
        mShaderMotionBlur.setUniform1i("usize", mBlurSize);

        mFboMotionBlurBuffer[0].getTexture().draw(0, 0);

        mShaderMotionBlur.end();

        mFboMotionBlur.end();

        mIndexMotionBlur++;
        mIndexMotionBlur %= mBlurSize;
    }

    // RENDER BLUR RESULT
#ifdef TEXTURE_BLUR
    {
        ofFbo* inResult = &mFboMotionBlur;

        //----------------------------------------------------------
        mFboTextureBlur.begin();

        mShaderTextureBlur.begin();
        mShaderTextureBlur.setUniformTexture("tex0", mImage.getTexture(), 1);
        mShaderTextureBlur.setUniformTexture("tex1", inResult->getTexture(), 2);
        mShaderTextureBlur.setUniform1f("ublurAmnt", mBlurFactor);
        for (int i = 0 ; i < sTextureBlurSize ; ++i) {
            string num = ofToString(i);
            mShaderTextureBlur.setUniformTexture("texblur" + num, mTextures[i], i + 3);
        }

        mImage.draw(0, 0);

        mShaderTextureBlur.end();

        mFboTextureBlur.end();
    }
#else
    {
        ofFbo* inResult = &mFboMotionBlur;

        //--------------------------------------------------------
        mImage.getTexture().bind(0);
        inResult->getTexture().bind(1);

        //----------------------------------------------------------
        mFboBlurOnePass.begin();

        mShaderBlurX.begin();
        mShaderBlurX.setUniformTexture("tex0", mImage.getTexture(), 0);
        mShaderBlurX.setUniformTexture("tex1", inResult->getTexture(), 1);
        mShaderBlurX.setUniform1f("ublurAmnt", mBlurFactor);
        mShaderBlurX.setUniform1f("usigma", mSigma);

        mImage.draw(0, 0);

        mShaderBlurX.end();

        mFboBlurOnePass.end();

        //----------------------------------------------------------
        mFboBlurTwoPass.begin();

        mShaderBlurY.begin();
        mShaderBlurY.setUniformTexture("tex0", mImage.getTexture(), 0);
        mShaderBlurY.setUniformTexture("tex1", inResult->getTexture(), 1);
        mShaderBlurY.setUniform1f("ublurAmnt", mBlurFactor);
        mShaderBlurY.setUniform1f("usigma", mSigma);

        mFboBlurOnePass.draw(0, 0);

        mShaderBlurY.end();

        mFboBlurTwoPass.end();

        //--------------------------------------------------------
        mImage.getTexture().unbind();
        inResult->getTexture().unbind();
    }
    //----------------------------------------------------------
#endif
    switch(mSelectRender)
    {
        case 0:
            ofSetColor(ofColor::white);
            mImage.getTexture().draw(0, 0, s.x, s.y);
        break;

        case 1:
            ofSetColor(ofColor::white);
            mFboDephMap.getTexture().draw(0, 0, s.x, s.y);
        break;

        case 2:
            ofSetColor(ofColor::white);
            mFboMirror.getTexture().draw(0, 0, s.x, s.y);
        break;

        case 3:
            ofSetColor(ofColor::white);
            mFboFilterDilEro2.getTexture().draw(0, 0, s.x, s.y);
        break;

        case 4:
            ofSetColor(ofColor::white);
            mFboFilterTwoPass.getTexture().draw(0, 0, s.x, s.y);
        break;

        case 5:
            ofSetColor(ofColor::white);
            mFboMotionBlur.getTexture().draw(0, 0, s.x, s.y);
        break;

        case 6:
            ofSetColor(ofColor::white);
            mFboMotionBlurBuffer[3].getTexture().draw(0, 0, s.x, s.y);
        break;

        case 7:
            ofSetColor(ofColor::white);
            #ifdef TEXTURE_BLUR
            mFboTextureBlur.getTexture().draw(0, 0, s.x, s.y);
            #else
            mFboBlurTwoPass.getTexture().draw(0, 0, s.x, s.y);
            #endif
        break;
    }
}

void ofBlur::loadXml() {
    mXml.loadFile("blur.xml");
    mStepf = mXml.getValue("settings:blur:steff", 0.1f);

    mBlurFilterFactor = mXml.getValue("settings:blur:blurfilterfactor", 0.f);
    mMirrorOffsetY = mXml.getValue("settings:blur:mirroroffsety", -1.f);
    mSigmaFilter = mXml.getValue("settings:blur:sigmafilter", 10.f);
    mErodeFilterFactor = mXml.getValue("settings:blur:erodefilter", 5.f);
    mDilateFilterFactor = mXml.getValue("settings:blur:dilatefilter", 0.f);
    mBlurFactor = mXml.getValue("settings:blur:blurfactor", 0.f);
    mSigma = mXml.getValue("settings:blur:sigmagaussian", 20.f);
	mScaleRender = mXml.getValue("settings:blur:scalerender", 1.f);
    mSelectRender = mXml.getValue("settings:blur:selectrender", 7);
    mSelectRenderResult = mXml.getValue("settings:blur:selectrenderresult", 6);
    mDephtSizeFactor = mXml.getValue("settings:blur:depthsizefactor", 1.f);
    mBlurSize = mXml.getValue("settings:blur:blursize", 4.f);
    mVisible = mXml.getValue("settings:blur:visible", false);
}

void ofBlur::saveXml() {
    mXml.setValue("settings:blur:steff", mStepf);
    mXml.setValue("settings:blur:blurfilterfactor", mBlurFilterFactor);
    mXml.setValue("settings:blur:mirroroffsety", mMirrorOffsetY);
    mXml.setValue("settings:blur:sigmafilter", mSigmaFilter);
    mXml.setValue("settings:blur:erodefilter", mErodeFilterFactor);
    mXml.setValue("settings:blur:dilatefilter", mDilateFilterFactor);
    mXml.setValue("settings:blur:blurfactor", mBlurFactor);
    mXml.setValue("settings:blur:sigmagaussian", mSigma);
	mXml.setValue("settings:blur:scalerender", mScaleRender);
    mXml.setValue("settings:blur:selectrender", mSelectRender);
    mXml.setValue("settings:blur:selectrenderresult", mSelectRenderResult);
    mXml.setValue("settings:blur:depthsizefactor", mDephtSizeFactor);
    mXml.setValue("settings:blur:blursize", mBlurSize);
    mXml.setValue("settings:blur:visible", mVisible);

    mXml.save("blur.xml");
}

//--------------------------------------------------------------
void ofBlur::drawGui()
{
    ImGui::SetNextWindowPos(ImVec2(100, 400.f), ImGuiCond_FirstUseEver);
    if (!ImGui::Begin("Blur window", p_open))
    {
        ImGui::End();
        return;
    }

    if(ImGui::Button("Load")) {
        loadXml();
    }
    ImGui::SameLine();
    if(ImGui::Button("Save")) {
        saveXml();
    }

    ImGui::Text("BLUR");
    ImGui::Checkbox("Visible", &mVisible);
    const float stepf = 0.01f;
    ImGui::InputScalar("Stepf", ImGuiDataType_Float, &mStepf, &stepf);
    if (ImGui::InputScalar("Depth size factor", ImGuiDataType_Float, &mDephtSizeFactor, &mStepf))
        allocateFbos();

    ImGui::SliderFloat("Mirror offset Y", &mMirrorOffsetY, -1.f, 5000.f);

    ImGui::SliderFloat("Blur filter factor", &mBlurFilterFactor, 0.f, 1000.f);
    ImGui::SliderFloat("Sigma filter gaussian", &mSigmaFilter, 0.f, 100.f);

    ImGui::SliderInt("Motion blur size", &mBlurSize, 1, 8);

    ImGui::SliderFloat("Erode filter factor", &mErodeFilterFactor, 0.f, 100.f);
    ImGui::SliderFloat("Dilate filter factor", &mDilateFilterFactor, 0.f, 100.f);

    ImGui::Separator();
    ImGui::InputScalar("Blur factor", ImGuiDataType_Float, &mBlurFactor, &mStepf);

    ImGui::Separator();
	ImGui::InputScalar("Scale", ImGuiDataType_Float, &mScaleRender, &mStepf);
    ImGui::SliderInt("Select render", &mSelectRender, 0, 7);
    ImGui::SliderInt("Select render result", &mSelectRenderResult, 0, 7);

    ImGui::End();
}
