#pragma once

#include "ofMain.h"

#include "ofxXmlSettings.h"

class ofCam
{
public:
    ofCam();
    ~ofCam();
    void reset();
    void loadXml();
    void saveXml();
    static void setupPers(ofCam& cam);
    static void setupTop(ofCam& cam);
    static void setupLeft(ofCam& cam);
    static void setupFront(ofCam& cam);
    static void setupDephMapOrtho(ofCam& cam);
    static void setupDephMap(ofCam& cam);
    static void resizePers(ofCam& cam);
    static void resizeTop(ofCam& cam);
    static void resizeLeft(ofCam& cam);
    static void resizeFront(ofCam& cam);
    static void resizeDephMap(ofCam& cam);
    void begin();
    void beginNoRect();
    void begin(const ofRectangle& rect);
    void end();
    void update(bool);
    void draw(const std::string& text);
    void drawGui();
    bool getVisibleMesh() { return mVisibleMesh; }
    glm::vec3 getScale() { return mScale; }
public:
    glm::vec3               mPosition;
    glm::vec3               mTarget;
    glm::vec3               mPositionReset;
    glm::vec3               mTargetReset;
    ofEasyCam               mCam;
    static ofCamera         mCamOrtho;

    ofRectangle             mRectangle;
    string                  mName;
    bool                    *p_open;
    bool                    mControlCam;
    bool                    mVisibleMesh;
    ofxXmlSettings          mXml;
    static unsigned int     sCount;
    unsigned int            sUniqueId;
    float                   mStepf;

    glm::vec3               mScale;
    float                   mRangeFactor;
};
