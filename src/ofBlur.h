#pragma once

#include "ofMain.h"
#include "ofxXmlSettings.h"

#define TEXTURE_BLUR

class ofBlur
{
public:
    ofBlur();
    ~ofBlur();
    void setup();
    void draw();
    void drawResult(const glm::vec2& scale, const glm::vec2& position);
    void drawGui();
    void dephBegin();
    void dephEnd();
    glm::vec2 getSize()
    {
        return mSize;
    }
    void loadXml();
    void saveXml();
    void allocateFbos();
private:

    // blur

#ifdef TEXTURE_BLUR
    static const int        sTextureBlurSize = 12; //12;
    ofShader                mShaderTextureBlur;
    ofFbo                   mFboTextureBlur;
    ofTexture               mTextures[sTextureBlurSize];
#else
    ofShader                mShaderBlurX;
    ofShader                mShaderBlurY;
    ofFbo                   mFboBlurOnePass;
    ofFbo                   mFboBlurTwoPass;
#endif


    // filter
    ofShader                mShaderMirror;
    ofShader                mShaderFilterX;
    ofShader                mShaderFilterY;
    ofShader                mShaderFilterDilEro;

    ofFbo                   mFboMirror;
    ofFbo                   mFboFilterOnePass;
    ofFbo                   mFboFilterTwoPass;
    ofFbo                   mFboFilterDilEro;
    ofFbo                   mFboFilterDilEro2;

    // motion blur
    int                     mBlurSize;
    ofFbo                   mFboMotionBlurBuffer[8];
    ofFbo                   mFboMotionBlur;
    int                     mIndexMotionBlur;
    ofShader                mShaderMotionBlur;

    // fbo deph map
    ofFbo                   mFboDephMap;

    ofImage                 mImage;

    bool                    *p_open;

    float                   mMirrorOffsetY;
    float                   mBlurFilterFactor;
    float                   mBlurFactor;
    float                   mSigma;
    float                   mSigmaFilter;
    float                   mErodeFilterFactor;
    float                   mDilateFilterFactor;

    glm::vec2               mSize;

	float					mScaleRender;
    int                     mSelectRender;
    int                     mSelectRenderResult;

    bool                    mVisible;

    // xml
    ofxXmlSettings          mXml;

    float                   mDephtSizeFactor;
    float                   mStepf;
};

