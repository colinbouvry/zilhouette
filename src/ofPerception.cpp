#include "ofPerception.h"

unsigned int     ofPerception::sCount = 0;
unsigned int     ofPerception::sMaxUniqueId = 0;

ofPerception::ofPerception()
    : mCamMesh(nullptr)
    , Objet()
    , mActive(false)
    , mLaunch(false)
{
    reset();

    sCount++;
    sUniqueId = sCount;
    if(sUniqueId > sMaxUniqueId)
        sMaxUniqueId = sUniqueId;
    p_open = new bool(true);

    loadXml();
}

ofPerception::ofPerception(unsigned int id)
    : mCamMesh(nullptr)
    , Objet()
    , mActive(false)
{
    reset();

    sCount++;
    sUniqueId = id;
    if(sUniqueId > sMaxUniqueId)
        sMaxUniqueId = sUniqueId;
    p_open = new bool(true);

    loadXml();
}

ofPerception::~ofPerception()
{
    if(mCamMesh)
           delete mCamMesh;
    rosKill();
}

void ofPerception::deletePerception(vector<ofPerception*>& perceptions, unsigned int &id) {

    auto it = perceptions.begin();
        while (it != perceptions.end())
        {
            // remove odd numbers
            if ((*it)->getUniqueID() == id ) {
                delete *it;
                // erase() invalidates the iterator, use returned iterator
                it = perceptions.erase(it);
            }
            // Notice that iterator is incremented only on the else part (why?)
            else {
                ++it;
            }
        }
}

void ofPerception::reset() {
    mThresholdConfidence = 200.f;
    mAngle = glm::vec3(0.f);
    mPosition = glm::vec3(0.f);
    mScale = glm::vec3(1.f, -1.f, 1.f);
    mPivot = glm::vec3(0.f);
    mVisible = true;
    mStepf = 1.f;
    mColor = ImColor(114.f/255.f, 144.f/255.f, 154.f/255.f, 200.f/255.f);
}

void ofPerception::loadXml() {
    mXml.loadFile("perceptions_" + ofToString(sUniqueId) + ".xml");

    mRosSub = mXml.getValue("settings:rossub", "/swissranger" + ofToString(sUniqueId) +"/pointcloud_raw");
    mType = mXml.getValue("settings:type", 1);

    mStepf = mXml.getValue("settings:stepf", 1.0);

    mAngle.x = mXml.getValue("settings:angle:x", 0.0);
    mAngle.y = mXml.getValue("settings:angle:y", 0.0);
    mAngle.z = mXml.getValue("settings:angle:z", 0.0);
    mPosition.x = mXml.getValue("settings:position:x", 0.0);
    mPosition.y = mXml.getValue("settings:position:y", 0.0);
    mPosition.z = mXml.getValue("settings:position:z", 0.0);

    mScale.x = mXml.getValue("settings:scale:x", 1.0);
    mScale.y = mXml.getValue("settings:scale:y", -1.0);
    mScale.z = mXml.getValue("settings:scale:z", 1.0);
    mPivot.x = mXml.getValue("settings:pivot:x", 0.0);
    mPivot.y = mXml.getValue("settings:pivot:y", 0.0);
    mPivot.z = mXml.getValue("settings:pivot:z", 0.0);

    mThresholdConfidence = mXml.getValue("settings:thresholdconfidence", 200.0);
    mColor.x = mXml.getValue("settings:color:x", 114.f/255.f);
    mColor.y = mXml.getValue("settings:color:y", 144.f/255.f);
    mColor.z = mXml.getValue("settings:color:z", 154.f/255.f);
    mColor.w = mXml.getValue("settings:color:w", 200.f/255.f);
    mColorMode = mXml.getValue("settings:colormode", false);
    mVisible = mXml.getValue("settings:visible", true);

    mPointCloudAverageFactor = mXml.getValue("settings:pointcloudaveragefactor", 0.1f);

    mMinFilter.x = mXml.getValue("settings:filter:minx", -20.f);
    mMinFilter.y = mXml.getValue("settings:filter:miny", -20.f);
    mMinFilter.z = mXml.getValue("settings:filter:minz", -20.f);
    mMaxFilter.x = mXml.getValue("settings:filter:maxx", 20.f);
    mMaxFilter.y = mXml.getValue("settings:filter:maxy", 20.f);
    mMaxFilter.z = mXml.getValue("settings:filter:maxz", 20.f);
}

void ofPerception::saveXml() {
    mXml.setValue("settings:rossub", mRosSub);
    mXml.setValue("settings:type", mType);

    mXml.setValue("settings:stepf", mStepf);

    mXml.setValue("settings:angle:x", mAngle.x);
    mXml.setValue("settings:angle:y", mAngle.y);
    mXml.setValue("settings:angle:z", mAngle.z);
    mXml.setValue("settings:position:x", mPosition.x);
    mXml.setValue("settings:position:y", mPosition.y);
    mXml.setValue("settings:position:z", mPosition.z);

    mXml.setValue("settings:scale:x", mScale.x);
    mXml.setValue("settings:scale:y", mScale.y);
    mXml.setValue("settings:scale:z", mScale.z);
    mXml.setValue("settings:pivot:x", mPivot.x);
    mXml.setValue("settings:pivot:y", mPivot.y);
    mXml.setValue("settings:pivot:z", mPivot.z);

    mXml.setValue("settings:thresholdconfidence", mThresholdConfidence);
    mXml.setValue("settings:color:x", mColor.x);
    mXml.setValue("settings:color:y", mColor.y);
    mXml.setValue("settings:color:z", mColor.z);
    mXml.setValue("settings:color:w", mColor.w);
    mXml.setValue("settings:colormode", mColorMode);
    mXml.setValue("settings:visible", mVisible);

    mXml.setValue("settings:pointcloudaveragefactor", mPointCloudAverageFactor);


    mXml.setValue("settings:filter:minx", mMinFilter.x);
    mXml.setValue("settings:filter:miny", mMinFilter.y);
    mXml.setValue("settings:filter:minz", mMinFilter.z);
    mXml.setValue("settings:filter:maxx", mMaxFilter.x);
    mXml.setValue("settings:filter:maxy", mMaxFilter.y);
    mXml.setValue("settings:filter:maxz", mMaxFilter.z);


    mXml.save("perceptions_" + ofToString(sUniqueId) + ".xml");
}

//--------------------------------------------------------------
#ifdef ZILHOUETTE_ROS
	void ofPerception::setup(ros::NodeHandle& handle/*, const string& ros_sub, int type*/){
#else
	void ofPerception::setup() {
#endif
/*    mRosSub = ros_sub;
    mType = type;
*/
#ifdef ZILHOUETTE_ROS
    rosLaunch();
    mHandle = &handle;
    // ROS SUBSCRIBE
    if(mType == 1)
        mPointCloudSub = mHandle->subscribe(mRosSub, 1, &ofPerception::cloudCallback, this);
    else if(mType == 2)
        mPointCloudSub = mHandle->subscribe(mRosSub, 1, &ofPerception::cloud2Callback, this);
#endif

    // MESH
    // we're going to load a ton of points into an ofMesh
    mesh.setMode(OF_PRIMITIVE_POINTS);
    mesh.setUsage(GL_DYNAMIC_DRAW);
    mesh.disableTextures();
    mesh.disableColors();
    mesh.disableNormals();
    mesh.enableIndices();

    // cam mesh
    mCamMesh = new ofConePrimitive(0.02f,0.1f, 5, 5);

    // init
    mLastActiveSecond = ofGetElapsedTimef();
    mLastKillSecond = ofGetElapsedTimef();
}

// TO DO USE ONLY VERTEX AND VBO
#ifdef ZILHOUETTE_ROS
void ofPerception::cloudCallback(const sensor_msgs::PointCloudConstPtr& msg) {
    size_t count = msg->points.size();
    if(count > 0) {
        mActive = true;
        mLastActiveSecond = ofGetElapsedTimef();
    }
    if(!mVisible)
        return;
    //ofColor colorPicker(mColor);
    //if(mesh.getNumIndices() != count)
    {
        mesh.clear();
        size_t i = 0;
        for (size_t i = 0 ; i < count ; ++i)
        {
            float conf = msg->channels[1].values[i] / 256.f; // 16 bits
            auto& pt = msg->points[i];
            glm::vec3 pos(pt.x, pt.y, pt.z);
            bool f = false;
            if(pos.x < mMinFilter.x) f = true;
            else if(pos.y < mMinFilter.y) f = true;
            else if(pos.z < mMinFilter.z) f = true;
            else if(pos.x > mMaxFilter.x) f = true;
            else if(pos.y > mMaxFilter.y) f = true;
            else if(pos.z > mMaxFilter.z) f = true;
            else if(conf < mThresholdConfidence) f = true;

            if(!f)
            {
                mesh.addVertex(pos);
                mesh.addColor(ofColor(255,255,255,255));
                mesh.addIndex(i);
            }
        }
    }
    /*
    else {
        for (size_t i = 0 ; i < count ; ++i)
        {
            float conf = msg->channels[1].values[i] / 256.f; // 16 bits
            auto& pt = msg->points[i];
            glm::vec3 pos(pt.x, pt.y, pt.z);
            bool f = false;
            if(pos.x < mMinFilter.x) f = true;
            else if(pos.y < mMinFilter.y) f = true;
            else if(pos.z < mMinFilter.z) f = true;
            else if(pos.x > mMaxFilter.x) f = true;
            else if(pos.y > mMaxFilter.y) f = true;
            else if(pos.z > mMaxFilter.z) f = true;
            else if(conf < mThresholdConfidence) f = true;

            glm::vec3 pos_old = mesh.getVertex(i);
            if(f) {
                mesh.setVertex(i, glm::vec3(-1.f)); //
            }
            else {
                pos = (1.f - mPointCloudAverageFactor) * pos_old + mPointCloudAverageFactor * pos;
                mesh.setVertex(i, pos);
            }
        }
    }
    */
}

// TO DO USE ONLY VERTEX AND VBO
void ofPerception::cloud2Callback(const sensor_msgs::PointCloud2ConstPtr& msg) {
    size_t count = msg->height * msg->width;
    if(count > 0) {
        mActive = true;
        mLastActiveSecond = ofGetElapsedTimef();
    }
    if(!mVisible)
        return;
    sensor_msgs::PointCloud2ConstIterator<float> in_x(*msg, "x");
    sensor_msgs::PointCloud2ConstIterator<float> in_y(*msg, "y");
    sensor_msgs::PointCloud2ConstIterator<float> in_z(*msg, "z");
    sensor_msgs::PointCloud2ConstIterator<uint8_t> iter_rgb(*msg, "rgb");

    size_t mesh_count = mesh.getNumIndices();

    mesh.clear();
    for (size_t i = 0; i < count ; ++i, ++in_x, ++in_y, ++in_z, ++iter_rgb)
    {
        glm::vec3 pos(*in_x, *in_y, *in_z);
        bool f = false;
        if(pos.x < mMinFilter.x) f = true;
        else if(pos.y < mMinFilter.y) f = true;
        else if(pos.z < mMinFilter.z) f = true;
        else if(pos.x > mMaxFilter.x) f = true;
        else if(pos.y > mMaxFilter.y) f = true;
        else if(pos.z > mMaxFilter.z) f = true;
        //else if(conf < mThresholdConfidence) f = true;
        if(!f) {
            mesh.addVertex(pos);
            mesh.addColor(ofColor(255,255,255,255));
            //mesh.addColor(ofColor(iter_rgb[0], iter_rgb[1], iter_rgb[2], 255));
        }
    }
}
#endif

void ofPerception::computeMat()
{
    mMat = ofMatrix4x4();
    mMat.preMultTranslate(mPosition);
    mMat.preMultRotate(ofQuaternion(mAngle.x, ofVec3f(1, 0, 0), mAngle.y, ofVec3f(0, 1, 0), mAngle.z, ofVec3f(0, 0, 1)));
    mMat.preMultScale(mScale);
    mMat.preMultTranslate(mPivot);
}

void ofPerception::update()
{
    computeMat();

    if (ofGetElapsedTimef() - mLastActiveSecond > 60.f) {
        if(!mActive){
            rosKill();
            mLastKillSecond = ofGetElapsedTimef();
        }
        mActive = false;
        mLastActiveSecond = ofGetElapsedTimef();
    }
    if (!mLaunch && ofGetElapsedTimef() - mLastKillSecond > 30.f) {
        rosLaunch();
    }
}

// TO DO
// https://stackoverflow.com/questions/16131963/depth-as-distance-to-camera-plane-in-glsl
// https://forum.openframeworks.cc/t/how-to-bind-a-texture-to-ofvbo-correctly/28143
void ofPerception::drawMesh()
{
    if(!mVisible)
        return;

    size_t count = mesh.getNumVertices();
    if(count < 2)
        return;

    ofPushView();

        ofSetColor(ofColor(1.0, 1.0, 1.0, 1.0));

        ofMultMatrix(mMat);

        mesh.getVbo().draw(GL_POINTS, 0,  count - 1); // GL_POINTS

    ofPopView();
}

void ofPerception::draw()
{
    if(!mVisible)
        return;

    ofPushView();
    ofPushMatrix();
        ofSetColor(ofColor(1.0, 1.0, 1.0, 1.0));

        ofMultMatrix(mMat);

        ofSetColor(ofColor(mColor));

        // draw cam
        ofPushMatrix();
            ofRotateXDeg(90.f);
            ofScale(5.f);
            if(mCamMesh)
                mCamMesh->drawWireframe();
            //ofDrawArrow(glm::vec3(0.f), glm::vec3(0., 0.1f, 0.f), 0.01f);
            //ofSetColor(ofColor(255, 255, 255, 255));
            string label = "cam " + ofToString(sUniqueId);
            ofDrawBitmapString(label, glm::vec3(0.01f));
        ofPopMatrix();

        // draw pivot
        ofPushMatrix();
            ofTranslate(mPivot);
            ofDrawAxis(0.2f);
        ofPopMatrix();

    ofPopMatrix();
    ofPopView();
}
void ofPerception::drawInfos()
{
    ofPushMatrix();
    string label = "cam " + ofToString(sUniqueId);
    ofDrawBitmapString(label + (mActive?" : active":" : noactive"), glm::vec2(20.f,800.f + sUniqueId*20.f));
    ofPopMatrix();
}
void ofPerception::drawGui()
{
    if ((*p_open) == false)
        return;

    string sId = ofToString(sUniqueId);
    string title = "Perception Window " + sId;

    ImGui::SetNextWindowPos(ImVec2(100, 400.f + 800.f * sUniqueId), ImGuiCond_FirstUseEver);
    if (!ImGui::Begin(title.c_str(), p_open))
    {
        ImGui::End();
        return;
    }

    if(ImGui::Button("Load")) {
        loadXml();
    }
    ImGui::SameLine();
    if(ImGui::Button("Save")) {
        saveXml();
    }
    ImGui::SameLine();
    if(ImGui::Button("Reset")) {
        reset();
    }
    ImGui::SameLine();
    if(ImGui::Button("Del")) {
        ofNotifyEvent(eDel, sUniqueId, this);
    }
    ImGui::Separator();
    ImGui::Text(mRosSub.c_str());
    ImGui::SliderInt("Type", &mType, 1,2);
    if(ImGui::Button("Reload")) {

#ifdef ZILHOUETTE_ROS
        mPointCloudSub.shutdown();

        // ROS SUBSCRIBE
        if(mHandle) {
        if(mType == 1)
            mPointCloudSub = mHandle->subscribe(mRosSub, 1, &ofPerception::cloudCallback, this);
        else if(mType == 2)
            mPointCloudSub = mHandle->subscribe(mRosSub, 1, &ofPerception::cloud2Callback, this);
        }
#endif

    }
    ImGui::Separator();
    ImGui::Checkbox("Visible", &mVisible);
    ImGui::SliderFloat("Confidence", &mThresholdConfidence, 0.f, 255.f);
    ImGui::ColorEdit4("Color", (float*)&mColor);
    ImGui::Checkbox("Color mode", &mColorMode);
    ImGui::InputScalar("Average factor", ImGuiDataType_Float, &mPointCloudAverageFactor, &mStepf);
    ImGui::Separator();
    const float stepf = 0.01f;
    ImGui::InputScalar("Stepf", ImGuiDataType_Float, &mStepf, &stepf);
    ImGui::InputScalar("Angle X", ImGuiDataType_Float, &mAngle.x, &mStepf);
    ImGui::InputScalar("Angle Y", ImGuiDataType_Float, &mAngle.y, &mStepf);
    ImGui::InputScalar("Angle Z", ImGuiDataType_Float, &mAngle.z, &mStepf);
    ImGui::InputScalar("Pos X", ImGuiDataType_Float, &mPosition.x, &mStepf);
    ImGui::InputScalar("Pos Y", ImGuiDataType_Float, &mPosition.y, &mStepf);
    ImGui::InputScalar("Pos z", ImGuiDataType_Float, &mPosition.z, &mStepf);

    ImGui::InputScalar("Scale X", ImGuiDataType_Float, &mScale.x, &mStepf);
    ImGui::InputScalar("Scale Y", ImGuiDataType_Float, &mScale.y, &mStepf);
    ImGui::InputScalar("Scale z", ImGuiDataType_Float, &mScale.z, &mStepf);
    ImGui::InputScalar("Pivot X", ImGuiDataType_Float, &mPivot.x, &mStepf);
    ImGui::InputScalar("Pivot Y", ImGuiDataType_Float, &mPivot.y, &mStepf);
    ImGui::InputScalar("Pivot z", ImGuiDataType_Float, &mPivot.z, &mStepf);

    ImGui::Separator();
    ImGui::Text("FILTER");
    ImGui::InputScalar("Min x", ImGuiDataType_Float, &mMinFilter.x, &mStepf);
    ImGui::InputScalar("Min y", ImGuiDataType_Float, &mMinFilter.y, &mStepf);
    ImGui::InputScalar("Min z", ImGuiDataType_Float, &mMinFilter.z, &mStepf);

    ImGui::InputScalar("Max x", ImGuiDataType_Float, &mMaxFilter.x, &mStepf);
    ImGui::InputScalar("Max y", ImGuiDataType_Float, &mMaxFilter.y, &mStepf);
    ImGui::InputScalar("Ma#!/bin/bashx z", ImGuiDataType_Float, &mMaxFilter.z, &mStepf);

    ImGui::End();
}

void ofPerception::rosLaunch()
{
    mLaunch = true;
    string s = "roslaunch zilhouette niform_moma_" + ofToString(sUniqueId) + ".launch &";
    system(s.c_str());
}

void ofPerception::rosKill()
{
    mLaunch = false;
    string s = "rosrun zilhouette niform_moma_kill_" + ofToString(sUniqueId) + ".sh &";
    system(s.c_str());
}
