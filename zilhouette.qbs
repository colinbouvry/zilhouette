import qbs
import qbs.Process
import qbs.File
import qbs.FileInfo
import qbs.TextFile
import "../../../libs/openFrameworksCompiled/project/qtcreator/ofApp.qbs" as ofApp

Project{
    property string of_root: "../../.."

    ofApp {
        name: { return FileInfo.baseName(sourceDirectory) }

        files: [
            "../../../../../catkin_ws/src/zilhouette/launch/niform_moma_1.launch",
            "../../../../../catkin_ws/src/zilhouette/launch/niform_moma_2.launch",
            "../../../../../catkin_ws/src/zilhouette/launch/niform_moma_3.launch",
            "../../../../../catkin_ws/src/zilhouette/launch/niform_moma_4.launch",
            "../../../../../catkin_ws/src/zilhouette/launch/niform_moma_5.launch",
            "../../../../../catkin_ws/src/zilhouette/launch/niform_moma_6.launch",
            "../../../../../catkin_ws/src/zilhouette/launch/niform_moma_7.launch",
            "../../../../../catkin_ws/src/zilhouette/launch/niform_moma_8.launch",
            "../../../../../catkin_ws/src/zilhouette/launch/niform_moma_9.launch",
            "../../../../../catkin_ws/src/zilhouette/scripts/niform_moma_kill_1.sh",
            "../../../../../catkin_ws/src/zilhouette/scripts/niform_moma_kill_2.sh",
            "../../../../../catkin_ws/src/zilhouette/scripts/niform_moma_kill_3.sh",
            "../../../../../catkin_ws/src/zilhouette/scripts/niform_moma_kill_4.sh",
            "../../../../../catkin_ws/src/zilhouette/scripts/niform_moma_kill_5.sh",
            "../../../../../catkin_ws/src/zilhouette/scripts/niform_moma_kill_6.sh",
            "../../../../../catkin_ws/src/zilhouette/scripts/niform_moma_kill_7.sh",
            "../../../../../catkin_ws/src/zilhouette/scripts/niform_moma_kill_8.sh",
            "../../../../../catkin_ws/src/zilhouette/scripts/niform_moma_kill_9.sh",
            "bin/data/shadersGL3/shaderBlurX.frag",
            "bin/data/shadersGL3/shaderBlurX.vert",
            "bin/data/shadersGL3/shaderBlurY.frag",
            "bin/data/shadersGL3/shaderBlurY.vert",
            "bin/data/shadersGL3/shaderDepht.frag",
            "bin/data/shadersGL3/shaderDepht.vert",
            "bin/data/shadersGL3/shaderFilter2X.frag",
            "bin/data/shadersGL3/shaderFilter2X.vert",
            "bin/data/shadersGL3/shaderFilter2Y.frag",
            "bin/data/shadersGL3/shaderFilter2Y.vert",
            "bin/data/shadersGL3/shaderFilterDilEro.frag",
            "bin/data/shadersGL3/shaderFilterDilEro.vert",
            "bin/data/shadersGL3/shaderFilterX.frag",
            "bin/data/shadersGL3/shaderFilterX.vert",
            "bin/data/shadersGL3/shaderFilterY.frag",
            "bin/data/shadersGL3/shaderFilterY.vert",
            "bin/data/shadersGL3/shaderMirror.frag",
            "bin/data/shadersGL3/shaderMirror.vert",
            "bin/data/shadersGL3/shaderMotionBlur.frag",
            "bin/data/shadersGL3/shaderMotionBlur.vert",
            "bin/data/shadersGL3/shaderTextureBlur.frag",
            "bin/data/shadersGL3/shaderTextureBlur.vert",
            "src/objet.cpp",
            "src/objet.h",
            "src/ofArchi.cpp",
            "src/ofArchi.h",
            "src/ofBlur.cpp",
            "src/ofBlur.h",
            "src/ofCam.cpp",
            "src/ofCam.h",
            "src/ofLandmark.cpp",
            "src/ofLandmark.h",
            "src/ofPerception.cpp",
            "src/ofPerception.h",
            "src/main.cpp",
            "src/ofApp.cpp",
            "src/ofApp.h",
            "src/ofUtils.h",
            "src/zilhouette.h",
        ]

        of.addons: [
            'ofxGui',
            'ofxXmlSettings',
            'ofxImGui',
        ]

        // additional flags for the project. the of module sets some
        // flags by default to add the core libraries, search paths...
        // this flags can be augmented through the following properties:
        of.pkgConfigs: []       // list of additional system pkgs to include
        of.includePaths: ['/opt/ros/noetic/include', '/opt/ros/noetic/lib/']     // include search paths
        of.cFlags: []           // flags passed to the c compiler
        of.cxxFlags: ['/opt/ros/noetic/lib']         // flags passed to the c++ compiler
        of.linkerFlags: []      // flags passed to the linker
        of.defines: []          // defines are passed as -D to the compiler
                                // and can be checked with #ifdef or #if in the code
        of.frameworks: []       // osx only, additional frameworks to link with the project
        of.staticLibraries: []  // static libraries
        of.dynamicLibraries: ['/opt/ros/noetic/lib/libroslib.so', '/opt/ros/noetic/lib/libroscpp.so', '/opt/ros/noetic/lib/librosconsole.so', '/opt/ros/noetic/lib/libroscpp_serialization.so'] // dynamic libraries

        // other flags can be set through the cpp module: http://doc.qt.io/qbs/cpp-module.html
        // eg: this will enable ccache when compiling
        //
        //cpp.compilerWrapper: 'ccache'
        //cpp.includePaths: ['/opt/ros/noetic/include']

        Depends{
            name: "cpp"
        }

        // common rules that parse the include search paths, core libraries...
        Depends{
            name: "of"
        }

        // dependency with the OF library
        Depends{
            name: "openFrameworks"
        }
    }

    property bool makeOF: true  // use makfiles to compile the OF library
                                // will compile OF only once for all your projects
                                // otherwise compiled per project with qbs
    

    property bool precompileOfMain: false  // precompile ofMain.h
                                           // faster to recompile when including ofMain.h 
                                           // but might use a lot of space per project

    references: [FileInfo.joinPaths(of_root, "/libs/openFrameworksCompiled/project/qtcreator/openFrameworks.qbs")]
}
